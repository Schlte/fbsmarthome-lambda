﻿using StackExchange.Redis;
using System;

namespace FBSmartHome
{
    public sealed class Cache
    {
        public static readonly Cache Instance = new Cache();

        private readonly ConnectionMultiplexer redis;

        static Cache()
        {
        }

        private Cache()
        {
            try
            {
                redis = ConnectionMultiplexer.Connect(Environment.GetEnvironmentVariable("REDIS_CONNECTION"));
            }
            catch (Exception ex)
            {
                Logger.Instance.Debug("Exception while creating redis connection: {0}", ex);
            }
        }

        public void StoreToken(string token, string customerId)
        {
            try
            {
                IDatabase database = redis?.GetDatabase((int)Database.TOKEN);
                database?.StringSet(token, customerId, TimeSpan.FromMinutes(60), flags: CommandFlags.FireAndForget);
            }
            catch (Exception ex)
            {
                Logger.Instance.Debug("Exception while writing to cache: {0}", ex);
            }
        }

        public string CheckToken(string token)
        {
            if (redis?.IsConnected == true)
            {
                string customerId = null;
                try
                {
                    IDatabase database = redis?.GetDatabase((int)Database.TOKEN);
                    customerId = database?.StringGet(token);
                }
                catch (Exception ex)
                {
                    Logger.Instance.Debug("Exception while reading cache: {0}", ex);
                }

                if (customerId != null)
                {
                    Logger.Instance.Log("MONITORING|{0}|{1}|count|fbsmarthome.cache|#cache:hit",
                            DateTimeOffset.Now.ToUnixTimeSeconds(), 1);
                    return customerId;
                }
            }

            Logger.Instance.Log("MONITORING|{0}|{1}|count|fbsmarthome.cache|#cache:miss",
                    DateTimeOffset.Now.ToUnixTimeSeconds(), 1);
            return null;
        }
    }
}
