﻿namespace FBSmartHome
{
    using System.Collections.Generic;

    // Color definition
    public class ColorTemplate
    {
        public double Hue { get; set; } = 0;
        public List<double> Saturation { get; set; } = new List<double>(); 
    }
}