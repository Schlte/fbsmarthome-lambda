﻿using System;

namespace FBSmartHome
{
    public class Connection
    {
        public ConnectionNumber Number { get; set; } = ConnectionNumber.DEFAULT;

        public string FritzBoxUrl { get; set; } = string.Empty;

        public string FritzBoxUsername { get; set; } = string.Empty;

        public string FritzBoxPassword { get; set; } = string.Empty;

        public string FritzBoxSessionId { get; set; } = FritzBox.NOSID;

        public DateTime LastUpdatedDevices { get; set; } = DateTime.MinValue;

        public DateTime LastUpdatedSessionId { get; set; } = DateTime.MinValue;

        public ConnectionState LastConnectionState { get; set; } = ConnectionState.UNREACHABLE;

        public DateTime BlockedUntil { get; set; } = DateTime.MinValue;

        public bool Legacy { get; set; } = false;

        public bool UseIPv6 { get; set; } = false;
    }
}
