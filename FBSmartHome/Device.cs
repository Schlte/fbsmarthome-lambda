﻿namespace FBSmartHome
{
    using System.Collections.Generic;
    using System.Linq;

    // Device to specify device found on Fritz!Box
    // Device.Identifier is main identifier for Alexa
    public class Device
    {
        public string Identifier { get; set; } = string.Empty;
        public string Id { get; set; } = string.Empty;
        public bool IsGroup { get; set; } = false;
        public List<string> GroupMembers { get; set; } = new List<string>();
        public string Firmware { get; set; } = string.Empty;
        public string Manufacturer { get; set; } = string.Empty;
        public string ProductName { get; set; } = string.Empty;
        public List<DeviceClass> DeviceClasses { get; set; } = new List<DeviceClass>();

        public string Name { get; set; } = string.Empty;
        public bool Present { get; set; } = false;

        public bool Locked { get; set; } = false;
        public bool BatteryLow { get; set; } = false;

        public bool Alert { get; set; } = false;

        public bool SwitchedOn { get; set; } = false;
        public Mode OperationMode { get; set; } = Mode.None;

        public double Temperature { get; set; } = 0;
        public double TemperatureActualValue { get; set; } = 0;
        public double TemperatureSetpoint { get; set; } = 0;
        public double TemperatureSetback { get; set; } = 0;
        public double TemperatureComfort { get; set; } = 0;
        public double TemperatureOff { get; set; } = 253.0;
        public double TemperatureOn { get; set; } = 256.0;

        public double Power { get; set; } = 0;
        public double Energy { get; set; } = 0;

        public List<LightMode> LightModes { get; set; } = new List<LightMode>();
        public double Level { get; set; } = 0;
        public double Hue { get; set; } = 0;
        public double Saturation { get; set; } = 0;
        public List<double> ColorTemperatures { get; set; } = new List<double>();
        public List<ColorTemplate> Colors { get; set; } = new List<ColorTemplate>();

        public ConnectionNumber ConnectionNumber { get; set; } = ConnectionNumber.DEFAULT;
        public bool AdditionalSwitch { get; set; } = false;

        public bool Disabled { get; set; } = false;

        public void AddDeviceClass(DeviceClass deviceClass)
        {
            if (!IsDeviceClass(deviceClass))
            {
                DeviceClasses.Add(deviceClass);
            }
        }
        public void AddLightMode(LightMode lightMode)
        {
            if (!HasLightMode(lightMode))
            {
                LightModes.Add(lightMode);
            }
        }

        public void RemoveDeviceClass(DeviceClass deviceClass) => DeviceClasses.Remove(deviceClass);

        public bool IsDeviceClass(DeviceClass deviceClass) => DeviceClasses.Contains(deviceClass);

        public bool HasDeviceClass() => DeviceClasses.Count > 0;

        public bool HasLightMode(LightMode lightMode) => LightModes.Contains(lightMode);

        public double GetMinimumColorTemperature() => ColorTemperatures.Any() ? ColorTemperatures.Min() : 2700.0;

        public double GetMaximumColorTemperature() => ColorTemperatures.Any() ? ColorTemperatures.Max() : 6500.0;
    }
}