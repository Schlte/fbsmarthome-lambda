﻿namespace FBSmartHome
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using RKon.Alexa.NET46.Payloads;
    using RKon.Alexa.NET46.Response;
    using RKon.Alexa.NET46.Types;
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;

    public sealed class EventGateway
    {
        public static readonly EventGateway Instance = new EventGateway();

        private readonly HttpClient Client;

        static EventGateway()
        {
        }

        private EventGateway()
        {
            Client = new HttpClient()
            {
                Timeout = TimeSpan.FromSeconds(5)
            };
        }

        public async Task<bool> SendEventAsync(Customer customer, SmartHomeResponse smartHomeResponse)
        {
            Logger.Instance.Debug("Sending event to gateway {0}: {1}", customer.UserRegion.Trim(), smartHomeResponse.Event.Header.Name);

            string url;

            // Check if customer is Alexa user and get the correct gateway uri
            if (customer.UserRegion.Trim() == "EU")
            {
                url = "https://api.eu.amazonalexa.com/v3/events";
            }
            else if (customer.UserRegion.Trim() == "NA")
            {
                url = "https://api.amazonalexa.com/v3/events";
            }
            else
            {
                return false;
            }

            Logger.Instance.Debug("Using URL: {0}", url);

            // Check if token is outdated and refresh
            if (customer.TokenValidity < DateTime.UtcNow)
            {
                await OAuth.Instance.RefreshAccessTokenAsync(customer).ConfigureAwait(false);
                switch (smartHomeResponse.Event.Header.Name)
                {
                    case HeaderNames.DELETE_REPORT:
                        ((DeleteReportPayload)smartHomeResponse.Event.Payload).Scope.Token = customer.AccessToken;
                        break;
                }
            }

            string jsonData = JsonConvert.SerializeObject(smartHomeResponse, new StringEnumConverter());
            StringContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");
            Logger.Instance.Debug("JSON message to gateway: {0}", jsonData);

            HttpResponseMessage response = null;
            try
            {
                Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", customer.AccessToken);
                response = await Client.PostAsync(url, content).ConfigureAwait(false);
            }
            catch (HttpRequestException)
            {
                Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                response?.Dispose();
                return false;
            }
            catch (TaskCanceledException)
            {
                Logger.Instance.Debug("Http POST timed out.");
                response?.Dispose();
                return false;
            }
            catch (OperationCanceledException)
            {
                Logger.Instance.Debug("Http POST timed out.");
                response?.Dispose();
                return false;
            }
            catch (InvalidOperationException)
            {
                Logger.Instance.Debug("URI was not ok.");
                response?.Dispose();
                return false;
            }

            // If retrieving was successfull, get response
            if (response?.StatusCode == HttpStatusCode.Accepted)
            {
                Logger.Instance.Debug("Sending event was OK");
                return true;
            }
            // If skill is disabled for this user, delete region
            else if (response?.StatusCode == HttpStatusCode.Forbidden)
            {
                customer.UserRegion = string.Empty;
                return false;
            }
            else if (response?.StatusCode == HttpStatusCode.Unauthorized)
            {
                Logger.Instance.Debug("Unauthorized. Refresh token");
                // If token was refused, refresh access token
                await OAuth.Instance.RefreshAccessTokenAsync(customer).ConfigureAwait(false);

                // try again
                switch (smartHomeResponse.Event.Header.Name)
                {
                    case HeaderNames.DELETE_REPORT:
                        ((DeleteReportPayload)smartHomeResponse.Event.Payload).Scope.Token = customer.AccessToken;
                        break;
                }

                jsonData = JsonConvert.SerializeObject(smartHomeResponse, new StringEnumConverter());
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                try
                {
                    Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", customer.AccessToken);
                    response = await Client.PostAsync(url, content).ConfigureAwait(false);
                }
                catch (HttpRequestException)
                {
                    Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                    response?.Dispose();
                    return false;
                }
                catch (TaskCanceledException)
                {
                    Logger.Instance.Debug("Http POST timed out.");
                    response?.Dispose();
                    return false;
                }
                catch (OperationCanceledException)
                {
                    Logger.Instance.Debug("Http POST timed out.");
                    response?.Dispose();
                    return false;
                }
                catch (InvalidOperationException)
                {
                    Logger.Instance.Debug("URI was not ok.");
                    response?.Dispose();
                    return false;
                }

                // If retrieving was successfull, get response
                if (response?.StatusCode == HttpStatusCode.Accepted)
                {
                    Logger.Instance.Debug("Sending event was OK");
                    response?.Dispose();
                    return true;
                }
                else
                {
                    Logger.Instance.Debug("Could not load response. Error: {0}", response?.StatusCode);
                    response?.Dispose();
                    return false;
                }
            }
            else
            {
                Logger.Instance.Debug("Could not load response. Error: {0}", response?.StatusCode);
                response?.Dispose();
                return false;
            }
        }
    }
}