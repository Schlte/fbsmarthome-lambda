﻿namespace FBSmartHome
{
    using RKon.Alexa.NET46.JsonObjects;
    using RKon.Alexa.NET46.Payloads;
    using RKon.Alexa.NET46.Response;
    using RKon.Alexa.NET46.Types;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Sockets;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using System.Xml.Linq;

    public sealed class FritzBox : IFritzBox
    {
        public static readonly FritzBox Instance = new FritzBox();

        private readonly HttpClientHandler httpClientHandler;
        private readonly HttpClientHandler httpClientHandlerProxy;
        public HttpClient Client;
        public HttpClient ProxyClient;

        static FritzBox()
        {
        }

        private FritzBox()
        {
            httpClientHandler = new HttpClientHandler
            {
#pragma warning disable RCS1163 // Unused parameter.
                ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true,
#pragma warning restore RCS1163 // Unused parameter.
                UseProxy = false,
                UseCookies = false
            };

            double connectionTimeout = double.TryParse(Environment.GetEnvironmentVariable("CONNECTION_TIMEOUT"), out double timeout) ? timeout : 5.0;

            Client = new HttpClient(httpClientHandler)
            {
                Timeout = TimeSpan.FromSeconds(connectionTimeout)
            };

            NetworkCredential proxyCredentials = new NetworkCredential(
                Environment.GetEnvironmentVariable("PROXY_USER"),
                Environment.GetEnvironmentVariable("PROXY_PASS")
            );

            System.Net.WebProxy proxy = new System.Net.WebProxy(Environment.GetEnvironmentVariable("PROXY_URI"))
            {
                Credentials = proxyCredentials
            };

            httpClientHandlerProxy = new HttpClientHandler()
            {
#pragma warning disable RCS1163 // Unused parameter.
                ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true,
#pragma warning restore RCS1163 // Unused parameter.
                UseCookies = false,
                UseProxy = true,
                Proxy = proxy,
                PreAuthenticate = true,
                UseDefaultCredentials = false
            };

            ProxyClient = new HttpClient(httpClientHandlerProxy)
            {
                Timeout = TimeSpan.FromSeconds(connectionTimeout)
            };
        }

        public const string NOSID = "0000000000000000";
        private const string LOGINPAGE = "login_sid.lua";
        private const string SMARTHOMEPAGE = "webservices/homeautoswitch.lua";
        private const double UPDATETIMESESSIONID = 20;

        private Uri GetIpAddress(Connection connection)
        {
            try
            {
                UriBuilder uri = new UriBuilder(connection.FritzBoxUrl);
                IPAddress[] ipAddresses = Dns.GetHostAddresses(uri.Host);

                foreach (IPAddress ipAddress in ipAddresses)
                {
                    if (!connection.UseIPv6 && ipAddress.AddressFamily == AddressFamily.InterNetwork)
                    {
                        uri.Host = ipAddress.ToString();
                        Logger.Instance.Debug("IPv4 Address found: {0}", uri.ToString());
                        return uri.Uri;
                    }
                    else if (connection.UseIPv6 && ipAddress.AddressFamily == AddressFamily.InterNetworkV6)
                    {
                        uri.Host = ipAddress.ToString();
                        Logger.Instance.Debug("IPv6 Address found: {0}", uri.ToString());
                        return uri.Uri;
                    }
                }

                Logger.Instance.Debug("IP Address not found");
                return null;
            }
            catch
            {
                Logger.Instance.Debug("Exception while resolving IP address");
                return null;
            }
        }

        // Login
        private async Task<ConnectionState> LoginAsync(Connection connection)
        {
            string fritzBoxAddress = connection.FritzBoxUrl;
            Uri uri = GetIpAddress(connection);
            if (uri != null)
            {
                fritzBoxAddress = uri.ToString();
            }

            // Try to read login page
            HttpResponseMessage response;

            try
            {
                string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + LOGINPAGE + "?sid=" + connection.FritzBoxSessionId;
                if (connection.UseIPv6)
                {
                    response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                }
                else
                {
                    response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                }
            }
            catch (HttpRequestException)
            {
                Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                return ConnectionState.UNREACHABLE;
            }
            catch (TaskCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                return ConnectionState.TIMEOUT;
            }
            catch (OperationCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                return ConnectionState.TIMEOUT;
            }

            // If retrieving was successfull, get response
            string responseString = string.Empty;
            if (response?.IsSuccessStatusCode == true)
            {
                try
                {
                    responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                }
                catch (InvalidOperationException)
                {
                    Logger.Instance.Debug(".net core bug occured");
                }

                Logger.Instance.Debug("Got response: {0}", responseString);
                response?.Dispose();
            }
            else
            {
                Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                response?.Dispose();
                return ConnectionState.UNREACHABLE;
            }

            // Get SID and block time
            string sid = GetXMLValue(responseString, "SID");
            string blocktimeString = GetXMLValue(responseString, "BlockTime");

            int time = int.TryParse(blocktimeString, out int blocktime) ? blocktime : 0;

            // Login is blocked
            if (time > 0)
            {
                Logger.Instance.Debug("Login blocked. Blocktime: {0}", time);

                connection.BlockedUntil = DateTime.UtcNow.AddSeconds(time + 10);
                return ConnectionState.BLOCKED;
            }

            if (string.IsNullOrEmpty(sid))
            {
                Logger.Instance.Debug("Login failed, not received SID");
                return ConnectionState.UNREACHABLE;
            }

            // SID not set, login
            if (sid.Equals(NOSID))
            {
                // Get challenge and login
                Logger.Instance.Debug("SID not set. Trying to log in");
                string challenge = GetXMLValue(responseString, "Challenge");
                string challengeResponse = CreateResponse(challenge, connection.FritzBoxPassword);
                try
                {
                    string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + LOGINPAGE + "?username=" + connection.FritzBoxUsername + "&response=" + challengeResponse;
                    if (connection.UseIPv6)
                    {
                        response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                    }
                    else
                    {
                        response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                    }
                }
                catch (HttpRequestException)
                {
                    Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                    return ConnectionState.UNREACHABLE;
                }
                catch (TaskCanceledException)
                {
                    Logger.Instance.Debug("Http GET timed out.");
                    return ConnectionState.TIMEOUT;
                }
                catch (OperationCanceledException)
                {
                    Logger.Instance.Debug("Http GET timed out.");
                    return ConnectionState.TIMEOUT;
                }

                // If retrieving was successfull, get response
                responseString = string.Empty;
                if (response?.IsSuccessStatusCode == true)
                {
                    try
                    {
                        responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    }
                    catch (InvalidOperationException)
                    {
                        Logger.Instance.Debug(".net core bug occured");
                    }

                    Logger.Instance.Debug("Got response: {0}", responseString);
                    response?.Dispose();
                }
                else
                {
                    Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                    response?.Dispose();
                    return ConnectionState.NOT_AUTHORIZED;
                }

                // Trying to read block time
                blocktimeString = GetXMLValue(responseString, "BlockTime");
                time = int.TryParse(blocktimeString, out blocktime) ? blocktime : 0;

                // Login is blocked
                if (time > 0)
                {
                    Logger.Instance.Debug("Login blocked. Blocktime: {0}", time);

                    connection.BlockedUntil = DateTime.UtcNow.AddSeconds(time + 10);
                    return ConnectionState.BLOCKED;
                }

                sid = GetXMLValue(responseString, "SID");
                if (sid.Equals(NOSID))
                {
                    Logger.Instance.Debug("Failed to log in.");
                    return ConnectionState.NOT_AUTHORIZED;
                }
            }

            Logger.Instance.Debug("Logged in with SID: {0}", sid);

            IEnumerable<XElement> rightsNames = XDocument.Parse(responseString).Root.Element("Rights").Elements("Name");
            IEnumerable<XElement> rightsAccesses = XDocument.Parse(responseString).Root.Element("Rights").Elements("Access");

            // Search for HomeAuto rights
            for (int i = 0; rightsNames.Skip(i).Any(); i++)
            {
                if (rightsNames.ElementAt(i).Value.Equals("HomeAuto"))
                {
                    bool writeAccess = rightsAccesses.ElementAtOrDefault(i)?.Value.Equals("2") == true;
                    Logger.Instance.Debug("Found HomeAuto right with write access: {0}", writeAccess);

                    if (writeAccess)
                    {
                        // All ok, logged in
                        Logger.Instance.Debug("All ok.");

                        // Update session id in db
                        connection.FritzBoxSessionId = sid;
                        connection.LastUpdatedSessionId = DateTime.UtcNow;

                        // Add custom metric for datadog logging
                        Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:login",
                            DateTimeOffset.Now.ToUnixTimeSeconds(), 1, connection.UseIPv6);

                        return ConnectionState.OK;
                    }
                }
            }

            // Not able to login or insufficient rights
            Logger.Instance.Debug("Rights not OK");
            return ConnectionState.NOT_AUTHORIZED;
        }

        // Log out
#pragma warning disable RCS1213 // Remove unused member declaration.
        private async Task LogoutAsync(Connection connection)
#pragma warning restore RCS1213 // Remove unused member declaration.
        {
            Logger.Instance.Debug("Logging out SID {0}", connection.FritzBoxSessionId);

            string fritzBoxAddress = connection.FritzBoxUrl;
            Uri uri = GetIpAddress(connection);
            if (uri != null)
            {
                fritzBoxAddress = uri.ToString();
            }

            if (!connection.FritzBoxSessionId.Equals(NOSID))
            {
                try
                {
                    string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + LOGINPAGE + "?logout=1";
                    if (connection.UseIPv6)
                    {
                        await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                    }
                    else
                    {
                        await Client.GetAsync(requestUri).ConfigureAwait(false);
                    }
                }
                catch (HttpRequestException)
                {
                    Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                }
                catch (TaskCanceledException)
                {
                    Logger.Instance.Debug("Http GET timed out.");
                }
                catch (OperationCanceledException)
                {
                    Logger.Instance.Debug("Http GET timed out.");
                }
            }
            connection.FritzBoxSessionId = NOSID;
        }

        // Parse device list according to AVM Home Automation HTTP Interface description
        // https://avm.de/fileadmin/user_upload/Global/Service/Schnittstellen/AHA-HTTP-Interface.pdf
        private Device ParseDevice(XElement deviceItem)
        {
            Device device = new Device
            {
                // Strip unicode characters to prevent exceptions on SNS publishing
                Identifier = StripUnicodeCharactersFromString(deviceItem.Attribute("identifier")?.Value.Replace(" ", string.Empty) ?? string.Empty),
                Id = StripUnicodeCharactersFromString(deviceItem.Attribute("id")?.Value ?? string.Empty),
                Firmware = StripUnicodeCharactersFromString(deviceItem.Attribute("fwversion")?.Value ?? string.Empty),
                //To show 'Waiting for Fritz!Box' in App
                Manufacturer = "FRITZ!Box", //deviceItem.Attribute("manufacturer")?.Value ?? String.Empty,
                ProductName = StripUnicodeCharactersFromString(deviceItem.Attribute("productname")?.Value ?? string.Empty),
                Name = StripUnicodeCharactersFromString(deviceItem.Element("name")?.Value ?? string.Empty),
                Present = deviceItem.Element("present")?.Value.Equals("1") ?? false
            };

            if (device.ProductName.Length == 0)
            {
                device.ProductName = "FRITZ!Box";
            }

            Logger.Instance.Debug("Found device: {0}", device.Name);

            int.TryParse(deviceItem.Attribute("functionbitmask")?.Value ?? string.Empty, out int bitmask);
            if (IsDeviceClass(bitmask, DeviceClass.AlarmSensor))
            {
                Logger.Instance.Debug("Device is Alarm Sensor");
                device.AddDeviceClass(DeviceClass.AlarmSensor);

                device.Alert = deviceItem.Element("alert")?.Element("state")?.Value.Equals("1") ?? false;
            }

            if (IsDeviceClass(bitmask, DeviceClass.DECTRepeater))
            {
                Logger.Instance.Debug("Device is Dect Repeater");
                device.AddDeviceClass(DeviceClass.DECTRepeater);
            }

            if (IsDeviceClass(bitmask, DeviceClass.Energymeter))
            {
                Logger.Instance.Debug("Device is Energy Meter");
                device.AddDeviceClass(DeviceClass.Energymeter);

                int power = int.TryParse(deviceItem.Element("powermeter")?.Element("power")?.Value,
                    out power) ? power : 0;
                device.Power = (power / 1000.0);

                int energy = int.TryParse(deviceItem.Element("powermeter")?.Element("energy")?.Value,
                    out energy) ? energy : 0;
                device.Energy = (energy / 1.0);
            }

            if (IsDeviceClass(bitmask, DeviceClass.PowerSwitch))
            {
                Logger.Instance.Debug("Device is Power Switch");
                device.AddDeviceClass(DeviceClass.PowerSwitch);

                device.SwitchedOn = deviceItem.Element("switch")?.Element("state")?.Value.Equals("1") ?? false;

                string mode = deviceItem.Element("switch")?.Element("mode")?.Value ?? string.Empty;
                if (mode.Equals("auto"))
                {
                    device.OperationMode = Mode.Auto;
                }
                else if (mode.Equals("manuell"))
                {
                    device.OperationMode = Mode.Manual;
                }

                device.Locked = deviceItem.Element("switch")?.Element("lock")?.Value.Equals("1") ?? false;
            }

            if (IsDeviceClass(bitmask, DeviceClass.TemperatureSensor))
            {
                Logger.Instance.Debug("Device is Temperature Sensor");
                device.AddDeviceClass(DeviceClass.TemperatureSensor);

                int temperature = int.TryParse(deviceItem.Element("temperature")?.Element("celsius")?.Value,
                    out temperature) ? temperature : 0;
                device.Temperature = (temperature / 10.0);
            }

            if (IsDeviceClass(bitmask, DeviceClass.Thermostat))
            {
                Logger.Instance.Debug("Device is Thermostat");
                device.AddDeviceClass(DeviceClass.Thermostat);

                int temperatureActualValue = int.TryParse(deviceItem.Element("hkr")?.Element("tist")?.Value,
                    out temperatureActualValue) ? temperatureActualValue : 0;
                if (temperatureActualValue == (int)Temperature.ON || temperatureActualValue == (int)Temperature.OFF)
                {
                    device.TemperatureActualValue = temperatureActualValue;
                }
                else
                {
                    device.TemperatureActualValue = temperatureActualValue / 2.0;
                }

                int temperatureSetpoint = int.TryParse(deviceItem.Element("hkr")?.Element("tsoll")?.Value,
                    out temperatureSetpoint) ? temperatureSetpoint : 0;
                if (temperatureSetpoint == (int)Temperature.ON || temperatureSetpoint == (int)Temperature.OFF)
                {
                    device.TemperatureSetpoint = temperatureSetpoint;
                }
                else
                {
                    device.TemperatureSetpoint = temperatureSetpoint / 2.0;
                }

                int temperatureComfort = int.TryParse(deviceItem.Element("hkr")?.Element("komfort")?.Value,
                    out temperatureComfort) ? temperatureComfort : 0;
                if (temperatureComfort == (int)Temperature.ON || temperatureComfort == (int)Temperature.OFF)
                {
                    device.TemperatureComfort = temperatureComfort;
                }
                else
                {
                    device.TemperatureComfort = temperatureComfort / 2.0;
                }

                int temperatureSetback = int.TryParse(deviceItem.Element("hkr")?.Element("absenk")?.Value,
                    out temperatureSetback) ? temperatureSetback : 0;
                if (temperatureSetback == (int)Temperature.ON || temperatureSetback == (int)Temperature.OFF)
                {
                    device.TemperatureSetback = temperatureSetback;
                }
                else
                {
                    device.TemperatureSetback = temperatureSetback / 2.0;
                }

                device.Locked = deviceItem.Element("hkr")?.Element("lock")?.Value.Equals("1") ?? false;
                device.BatteryLow = deviceItem.Element("hkr")?.Element("batterylow")?.Value.Equals("1") ?? false;
            }

            if (IsDeviceClass(bitmask, DeviceClass.Light))
            {
                Logger.Instance.Debug("Device is Light");
                device.AddDeviceClass(DeviceClass.Light);

                if(IsDeviceClass(bitmask, DeviceClass.SimpleOnOff))
                {
                    device.SwitchedOn = deviceItem.Element("simpleonoff")?.Element("state")?.Value.Equals("1") ?? false;
                }

                if (IsDeviceClass(bitmask, DeviceClass.WithLevel))
                {
                    device.AddDeviceClass(DeviceClass.WithLevel);

                    int level = int.TryParse(deviceItem.Element("levelcontrol")?.Element("level")?.Value, out level) ? level : 0;
                    device.Level = level / 255.0 * 100.0;
                }

                if (IsDeviceClass(bitmask, DeviceClass.WithColor))
                {
                    device.AddDeviceClass(DeviceClass.WithColor);

                    int supportBitmask = int.TryParse(deviceItem.Element("colorcontrol").Attribute("supported_modes")?.Value ?? string.Empty, out supportBitmask) ? supportBitmask : 0;
                    if (SupportsLightMode(supportBitmask, LightMode.SupportsHueAndSaturation))
                    {
                        device.AddLightMode(LightMode.SupportsHueAndSaturation);

                        double hue = double.TryParse(deviceItem.Element("colorcontrol")?.Element("hue")?.Value, out hue) ? hue : 0;
                        double saturation = double.TryParse(deviceItem.Element("colorcontrol")?.Element("saturation")?.Value, out saturation) ? saturation : 0;

                        device.Hue = hue;
                        device.Saturation = saturation / 255.0 * 100.0;
                    }

                    if (SupportsLightMode(supportBitmask, LightMode.SupportsColorTemperature))
                    {
                        device.AddLightMode(LightMode.SupportsColorTemperature);

                        double temperature = double.TryParse(deviceItem.Element("colorcontrol")?.Element("temperature")?.Value, out temperature) ? temperature : 0;

                        device.Temperature = temperature;
                    }
                }

                device.Locked = deviceItem.Element("switch")?.Element("lock")?.Value.Equals("1") ?? false;
            }

            if (IsDeviceClass(bitmask, DeviceClass.HanFun))
            {
                int unitType = int.TryParse(deviceItem.Element("etsiunitinfo")?.Element("unittype")?.Value, out unitType) ? unitType : 0;

                if (IsDeviceClass(bitmask, DeviceClass.WithLevel) && unitType == (int)DeviceClass.Blind)
                {
                    device.AddDeviceClass(DeviceClass.Blind);
                    device.AddDeviceClass(DeviceClass.WithLevel);

                    int level = int.TryParse(deviceItem.Element("levelcontrol")?.Element("level")?.Value, out level) ? level : 0;
                    device.Level = level / 255.0 * 100.0;
                }
                else if (IsDeviceClass(bitmask, DeviceClass.SimpleOnOff) && !IsDeviceClass(bitmask, DeviceClass.Light))
                {
                    device.AddDeviceClass(DeviceClass.SimpleOnOff);
                    device.SwitchedOn = deviceItem.Element("simpleonoff")?.Element("state")?.Value.Equals("1") ?? false;
                }
            }

            // Device is group, parse members
            if (deviceItem?.Element("groupinfo") != null)
            {
                device.IsGroup = true;
                string[] memberArray = deviceItem.Element("groupinfo")?.Element("members")?.Value.Split(',') ?? new string[0];
                memberArray = memberArray.Where(member => !string.IsNullOrEmpty(member)).ToArray();

                // prevent empty string set for dynamoDB
                if (memberArray.Length > 0)
                {
                    device.GroupMembers = memberArray.ToList<string>();
                }
            }

            return device;
        }

        private Device ParseTemplate(XElement templateItem)
        {
            Device device = new Device
            {
                // Strip unicode characters to prevent exceptions on SNS publishing
                Identifier = StripUnicodeCharactersFromString(templateItem.Attribute("identifier")?.Value.Replace(" ", string.Empty) ?? string.Empty),
                Id = StripUnicodeCharactersFromString(templateItem.Attribute("id")?.Value ?? string.Empty),
                //To show 'Waiting for Fritz!Box' in App
                Manufacturer = "FRITZ!Box",
                ProductName = "Template",
                Name = StripUnicodeCharactersFromString(templateItem.Element("name")?.Value ?? string.Empty),
                Present = true
            };
            device.AddDeviceClass(DeviceClass.Template);

            Logger.Instance.Debug("Found template: {0}", device.Name);

            return device;
        }

        // Pull Home Automation device list from Fritz!Box
        public async Task<ConnectionState> GetDevicesAsync(Customer customer, Connection connection, bool saveCustomer, bool forceUpdate)
        {
            // If devices were updated recently, do not update again
            // unless update is forced
            if (connection.LastUpdatedSessionId.AddMinutes(1) > DateTime.UtcNow && !forceUpdate)
    {
                return ConnectionState.OK;
            }

            // Fast fail if login is blocked
            if (connection.BlockedUntil > DateTime.UtcNow)
            {
                Logger.Instance.Debug("Login still blocked. Failing.");
                return ConnectionState.BLOCKED;
            }

            // If customer has legacy device, change to retrieving legacy switches
            if (connection.Legacy)
            {
                Logger.Instance.Debug("Legacy support active, do not update device list");
                // Do not update device list, it takes way too long
                return connection.LastConnectionState;
            }

            string fritzBoxAddress = connection.FritzBoxUrl;
            Uri uri = GetIpAddress(connection);
            if (uri != null)
            {
                fritzBoxAddress = uri.ToString();
            }

            HttpResponseMessage response = null;
            bool skipFirstTry = false;
            ConnectionState connectionState = ConnectionState.UNKNOWN;

            if (connection.LastUpdatedSessionId.AddMinutes(UPDATETIMESESSIONID) < DateTime.UtcNow)
            {
                skipFirstTry = true;
                Logger.Instance.Debug("Session outdated, skip first try");
            }

            // Try to read device info page
            try
            {
                if (!skipFirstTry)
                {
                    string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=getdevicelistinfos";
                    if (connection.UseIPv6)
                    {
                        response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                    }
                    else
                    {
                        response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                    }
                }
            }
            catch (HttpRequestException)
            {
                Logger.Instance.Debug("Could not load uri. Cannot proceed.");

                connectionState = ConnectionState.UNREACHABLE;
            }
            catch (TaskCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");

                connectionState = ConnectionState.TIMEOUT;
            }
            catch (OperationCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");

                connectionState = ConnectionState.TIMEOUT;
            }
            catch (InvalidOperationException)
            {
                Logger.Instance.Debug("URI was not ok.");
                connectionState = ConnectionState.UNREACHABLE;
            }

            // If retrieving was successfull, get response
            string responseString = string.Empty;
            if (response?.StatusCode == HttpStatusCode.OK)
            {
                connection.LastUpdatedSessionId = DateTime.UtcNow;
                connectionState = ConnectionState.OK;
                try
                {
                    responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                }
                catch (InvalidOperationException)
                {
                    Logger.Instance.Debug(".net core bug occured");
                    connectionState = ConnectionState.UNREACHABLE;
                }

                Logger.Instance.Debug("Got response: {0}", responseString);
                response?.Dispose();
            }
            else if (skipFirstTry || response?.StatusCode == HttpStatusCode.Forbidden)
            {
                // Forbidden: User not logged in
                Logger.Instance.Debug("Received status code FORBIDDEN. Try Login first");

                // Try to log in
                ConnectionState login = await LoginAsync(connection).ConfigureAwait(false);

                // If login was ok, try to set again
                if (login == ConnectionState.OK)
                {
                    try
                    {
                        string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId
                            + "&switchcmd=getdevicelistinfos";
                        if (connection.UseIPv6)
                        {
                            response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                        }
                        else
                        {
                            response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                        }

                        if (response?.StatusCode == HttpStatusCode.OK)
                        {
                            connectionState = ConnectionState.OK;
                            try
                            {
                                responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            }
                            catch (InvalidOperationException)
                            {
                                Logger.Instance.Debug(".net core bug occured");
                                connectionState = ConnectionState.UNREACHABLE;
                            }

                            Logger.Instance.Debug("Got response: {0}", responseString);
                            response?.Dispose();
                        }
                        else
                        {
                            Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                            response?.Dispose();

                            connectionState = ConnectionState.UNREACHABLE;
                        }
                    }
                    catch (HttpRequestException)
                    {
                        Logger.Instance.Debug("Could not load uri. Cannot proceed.");

                        connectionState = ConnectionState.UNREACHABLE;
                    }
                    catch (TaskCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");

                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (OperationCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");

                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (InvalidOperationException)
                    {
                        Logger.Instance.Debug("URI was not ok.");
                        connectionState = ConnectionState.UNREACHABLE;
                    }
                }
                else
                {
                    Logger.Instance.Debug("Could not log in");
                    response?.Dispose();

                    connectionState = login;
                }
            }
            else
            {
                Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                response?.Dispose();

                if (connectionState != ConnectionState.TIMEOUT)
                {
                    connectionState = ConnectionState.UNREACHABLE;
                }
            }

            if (connectionState == ConnectionState.OK)
            {
                XDocument document;
                try
                {
                    document = XDocument.Parse(responseString);
                }
                catch (XmlException)
                {
                    document = new XDocument();
                }

                string version = document?.Root?.Attribute("version")?.Value ?? "0";

                if (!version.Equals("1"))
                {
                    Logger.Instance.Debug("Not the correct device list version");

                    connectionState = ConnectionState.UNREACHABLE;
                }
                else
                {
                    List<Device> deviceList = new List<Device>();

                    IEnumerable<XElement> devices = document.Root.Elements("device");
                    foreach (XElement deviceItem in devices)
                    {
                        Device device = ParseDevice(deviceItem);
                        if (device.HasDeviceClass())
                        {
                            deviceList.Add(device);
                        }
                    }

                    IEnumerable<XElement> groups = document.Root.Elements("group");
                    foreach (XElement groupItem in groups)
                    {
                        Device group = ParseDevice(groupItem);
                        if(group.HasDeviceClass())
                        {
                            group.ProductName = "Gruppe";
                            deviceList.Add(group);
                        }
                    }

                    // Try to get templates
                    try
                    {
                        string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId
                            + "&switchcmd=gettemplatelistinfos";
                        if (connection.UseIPv6)
                        {
                            response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                        }
                        else
                        {
                            response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                        }

                        if (response?.StatusCode == HttpStatusCode.OK)
                        {
                            try
                            {
                                responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                                Logger.Instance.Debug("Got response: {0}", responseString);

                                try
                                {
                                    document = XDocument.Parse(responseString);
                                }
                                catch (XmlException)
                                {
                                    document = new XDocument();
                                }

                                version = document?.Root?.Attribute("version")?.Value ?? "0";

                                if (!version.Equals("1"))
                                {
                                    Logger.Instance.Debug("Not the correct template list version");
                                }
                                else
                                {
                                    IEnumerable<XElement> templates = document.Root.Elements("template");
                                    foreach (XElement templateItem in templates)
                                    {
                                        Device template = ParseTemplate(templateItem);
                                        deviceList.Add(template);
                                    }
                                }
                            }
                            catch (InvalidOperationException)
                            {
                                Logger.Instance.Debug(".net core bug occured");
                            }

                            response?.Dispose();
                        }
                        else
                        {
                            Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                            response?.Dispose();
                        }
                    }
                    catch (HttpRequestException)
                    {
                        Logger.Instance.Debug("Could not load uri. Cannot proceed.");

                        connectionState = ConnectionState.UNREACHABLE;
                    }
                    catch (TaskCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");

                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (OperationCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");

                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (InvalidOperationException)
                    {
                        Logger.Instance.Debug("URI was not ok.");
                        connectionState = ConnectionState.UNREACHABLE;
                    }

                    // transfer user input values if possible
                    foreach (Device device in deviceList)
                    {
                        // Set connection number
                        device.ConnectionNumber = connection.Number;

                        Device customerDevice = customer.DeviceList.Find(singleDevice => singleDevice.Identifier == device.Identifier);
                        if (customerDevice != null)
                        {
                            device.Disabled = customerDevice.Disabled;
                        }

                        if (device.IsDeviceClass(DeviceClass.Thermostat))
                        {
                            if (customerDevice != null)
                            {
                                device.TemperatureOff = customerDevice.TemperatureOff;
                                device.TemperatureOn = customerDevice.TemperatureOn;
                                device.AdditionalSwitch = customerDevice.AdditionalSwitch;
                            }
                        }
                        else if (device.IsDeviceClass(DeviceClass.Light))
                        {
                            // If device is offline, copy over light modes as FRITZ!Box sends empty 
                            // light modes for unavailable devices
                            if (!device.Present) 
                            {
                                if (customerDevice != null)
                                {
                                    device.LightModes = new List<LightMode>();
                                    foreach (LightMode lightMode in customerDevice.LightModes)
                                    {
                                        device.LightModes.Add(lightMode);
                                    }
                                }
                            }

                            // Try to get color templates
                            try
                            {
                                string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId
                                    + "&switchcmd=getcolordefaults";
                                if (connection.UseIPv6)
                                {
                                    response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                                }
                                else
                                {
                                    response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                                }

                                if (response?.StatusCode == HttpStatusCode.OK)
                                {
                                    try
                                    {
                                        responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                                        Logger.Instance.Debug("Got response: {0}", responseString);

                                        try
                                        {
                                            document = XDocument.Parse(responseString);
                                        }
                                        catch (XmlException)
                                        {
                                            document = new XDocument();
                                        }

                                        IEnumerable<XElement> colorTemperatures = document.Root.Element("temperaturedefaults").Elements("temp");
                                        foreach (XElement colorTemperature in colorTemperatures)
                                        {
                                            double temperature = double.TryParse(colorTemperature.Attribute("value")?.Value, out temperature) ? temperature : -1;
                                            if (temperature >= 0)
                                            {
                                                device.ColorTemperatures.Add(temperature);
                                            }
                                        }

                                        IEnumerable<XElement> hsColors = document.Root.Element("hsdefaults").Elements("hs");
                                        foreach (XElement hsColor in hsColors)
                                        {
                                            IEnumerable<XElement> colors = hsColor.Elements("color");
                                            if(colors.Count() <= 0)
                                            {
                                                continue;
                                            }

                                            ColorTemplate template = new ColorTemplate();

                                            double hue = double.TryParse(colors.First().Attribute("hue")?.Value, out hue) ? hue : -1;
                                            if(hue >= 0)
                                            {
                                                template.Hue = hue;
                                            }

                                            foreach(XElement color in colors)
                                            {
                                                double saturation = double.TryParse(color.Attribute("sat")?.Value, out saturation) ? saturation : -1;
                                                if(saturation >= 0)
                                                {
                                                    template.Saturation.Add(saturation);
                                                }
                                            }

                                            device.Colors.Add(template);
                                        }
                                    }
                                    catch (InvalidOperationException)
                                    {
                                        Logger.Instance.Debug(".net core bug occured");
                                    }

                                    response?.Dispose();
                                }
                                else
                                {
                                    Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                                    response?.Dispose();
                                }
                            }
                            catch (HttpRequestException)
                            {
                                Logger.Instance.Debug("Could not load uri. Cannot proceed.");

                                connectionState = ConnectionState.UNREACHABLE;
                            }
                            catch (TaskCanceledException)
                            {
                                Logger.Instance.Debug("Http GET timed out.");

                                connectionState = ConnectionState.TIMEOUT;
                            }
                            catch (OperationCanceledException)
                            {
                                Logger.Instance.Debug("Http GET timed out.");

                                connectionState = ConnectionState.TIMEOUT;
                            }
                            catch (InvalidOperationException)
                            {
                                Logger.Instance.Debug("URI was not ok.");
                                connectionState = ConnectionState.UNREACHABLE;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }

                    // Only save device list on customer if it is not empty. Fritzbox might have sent a faulty and empty list
                    if (deviceList.Count > 0)
                    {
                        // Check if a device was deleted. If so, send delete report to event gateway
                        SmartHomeResponse deleteReport = SmartHomeResponse.CreateDeleteReportEvent();
                        ((DeleteReportPayload)deleteReport.Event.Payload).Scope = new Scope(ScopeTypes.BearerToken, customer.AccessToken);

                        Logger.Instance.Debug("Device list contains {0} elements", deviceList.Count);
                        customer.RemoveDevices(connection.Number);

                        // remove devices which are duplicate in other connections
                        foreach (Device device in deviceList)
                        {
                            customer.RemoveDevice(device);
                        }

                        customer.DeviceList.AddRange(deviceList.ToList<Device>());

                        // if deleted endpoints were found, send message to event gateway
                        //if (((DeleteReportPayload)deleteReport.Event.Payload).Endpoints.Count > 0)
                        //{
                        //    await EventGateway.Instance.SendEventAsync(customer, deleteReport).ConfigureAwait(false);
                        //}
                    }

                    // Update time
                    connection.LastUpdatedDevices = DateTime.UtcNow;

                    connectionState = ConnectionState.OK;

                    // Add custom metric for datadog logging
                    Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:getdevices",
                        DateTimeOffset.Now.ToUnixTimeSeconds(), 1, connection.UseIPv6);
                }
            }

            // Delete devices with an unknown connection number
            foreach (Device device in customer.DeviceList)
            {
                if (!customer.IsConnection(device.ConnectionNumber))
                {
                    customer.RemoveDevice(device);
                }
            }

            // Save data in db if requested
            if (saveCustomer)
            {
                connection.LastConnectionState = connectionState;
                PostgreSQL.Instance.SaveCustomer(customer);
            }

            return connectionState;
        }

        /*
                // Get smart plugs from legacy devices
                public async Task<Connection> GetDevicesLegacyAsync(Customer customer)
                {
                    HttpResponseMessage response = null;
                    Connection connection = Connection.UNKNOWN;

                    Logger.Instance.Debug("Trying to get legacy switch list");
                    // Try to read switch list
                    try
                    {
                        string uri = customer.FritzBoxUrl.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + customer.FritzBoxSessionId + "&switchcmd=getswitchlist";
                        if (customer.UseIPv6)
                        {
                            response = await ProxyClient.GetAsync(uri).ConfigureAwait(false);
                        }
                        else
                        {
                            response = await Client.GetAsync(uri).ConfigureAwait(false);
                        }
                    }
                    catch (HttpRequestException)
                    {
                        Logger.Instance.Debug("Could not load uri. Cannot proceed.");

                        connection = Connection.UNREACHABLE;
                    }
                    catch (TaskCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");

                        connection = Connection.TIMEOUT;
                    }
                    catch (InvalidOperationException)
                    {
                        Logger.Instance.Debug("URI was not ok.");
                        connection = Connection.UNREACHABLE;
                    }

                    // If retrieving was successfull, get response
                    string responseString = string.Empty;
                    if (response?.StatusCode == HttpStatusCode.OK)
                    {
                        responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        Logger.Instance.Debug("Got response: {0}", responseString);
                        response?.Dispose();
                        connection = Connection.OK;
                    }
                    else if (response?.StatusCode == HttpStatusCode.Forbidden)
                    {
                        // Forbidden: User not logged in
                        Logger.Instance.Debug("Received status code FORBIDDEN. Try Login first");

                        // Try to log in
                        Connection login = await LoginAsync(customer).ConfigureAwait(false);

                        // If login was ok, try to set again
                        if (login == Connection.OK)
                        {
                            try
                            {
                                string uri = customer.FritzBoxUrl.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + customer.FritzBoxSessionId + "&switchcmd=getswitchlist";
                                if (customer.UseIPv6)
                                {
                                    response = await ProxyClient.GetAsync(uri).ConfigureAwait(false);
                                }
                                else
                                {
                                    response = await Client.GetAsync(uri).ConfigureAwait(false);
                                }

                                if (response?.StatusCode == HttpStatusCode.OK)
                                {
                                    responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                                    Logger.Instance.Debug("Got response: {0}", responseString);
                                    response?.Dispose();
                                    connection = Connection.OK;
                                }
                                else
                                {
                                    Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                                    response?.Dispose();

                                    connection = Connection.UNREACHABLE;
                                }
                            }
                            catch (HttpRequestException)
                            {
                                Logger.Instance.Debug("Could not load uri. Cannot proceed.");

                                connection = Connection.UNREACHABLE;
                            }
                            catch (TaskCanceledException)
                            {
                                Logger.Instance.Debug("Http GET timed out.");

                                connection = Connection.TIMEOUT;
                            }
                            catch (InvalidOperationException)
                            {
                                Logger.Instance.Debug("URI was not ok.");
                                connection = Connection.UNREACHABLE;
                            }
                        }
                        else
                        {
                            Logger.Instance.Debug("Could not log in");
                            response?.Dispose();

                            connection = login;
                        }
                    }
                    else
                    {
                        Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                        response?.Dispose();

                        if (connection != Connection.TIMEOUT)
                        {
                            connection = Connection.UNREACHABLE;
                        }
                    }

                    if (connection == Connection.OK)
                    {
                        //Retrieving switch list was ok. Try parsing list
                        string[] switchList = responseString.Split(',');

                        customer.DeviceList.Clear();
                        foreach (string singleSwitch in switchList)
                        {
                            singleSwitch = Regex.Replace(singleSwitch, @"\r\n?|\n", "");
                            Logger.Instance.Debug("Adding legacy device: {0}", singleSwitch);
                            Device device = new Device
                            {
                                Identifier = singleSwitch.Replace(" ", string.Empty),
                                Manufacturer = "FRITZ!Box"
                            };
                            device.AddDeviceClass(DeviceClass.PowerSwitch);

                            HttpResponseMessage responseName = null;
                            HttpResponseMessage responseState = null;
                            HttpResponseMessage responsePresent = null;

                            string responseStringName = string.Empty;
                            string responseStringState = string.Empty;
                            string responseStringPresent = string.Empty;
                            try
                            {
                                string uriName = customer.FritzBoxUrl.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid="
                                    + customer.FritzBoxSessionId + "&switchcmd=getswitchname" + "&ain=" + singleSwitch;
                                string uriState = customer.FritzBoxUrl.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid="
                                    + customer.FritzBoxSessionId + "&switchcmd=getswitchstate" + "&ain=" + singleSwitch;
                                string uriPresent = customer.FritzBoxUrl.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid="
                                    + customer.FritzBoxSessionId + "&switchcmd=getswitchpresent" + "&ain=" + singleSwitch;

                                if (customer.UseIPv6)
                                {
                                    responseName = await ProxyClient.GetAsync(uriName).ConfigureAwait(false);
                                    responseState = await ProxyClient.GetAsync(uriState).ConfigureAwait(false);
                                    responsePresent = await ProxyClient.GetAsync(uriPresent).ConfigureAwait(false);
                                }
                                else
                                {
                                    responseName = await Client.GetAsync(uriName).ConfigureAwait(false);
                                    responseState = await Client.GetAsync(uriState).ConfigureAwait(false);
                                    responsePresent = await Client.GetAsync(uriPresent).ConfigureAwait(false);
                                }

                                if (responseName?.StatusCode == HttpStatusCode.OK
                                    && responseState?.StatusCode == HttpStatusCode.OK
                                    && responsePresent?.StatusCode == HttpStatusCode.OK)
                                {
                                    responseStringName = await responseName.Content.ReadAsStringAsync().ConfigureAwait(false);
                                    responseStringState = await responseState.Content.ReadAsStringAsync().ConfigureAwait(false);
                                    responseStringPresent = await responsePresent.Content.ReadAsStringAsync().ConfigureAwait(false);

                                    responseStringName = Regex.Replace(responseStringName, @"\r\n?|\n", "");
                                    responseStringState = Regex.Replace(responseStringState, @"\r\n?|\n", "");
                                    responseStringPresent = Regex.Replace(responseStringPresent, @"\r\n?|\n", "");

                                    Logger.Instance.Debug("Got response: {0} - {1} - {2}", responseStringName, responseStringState, responseStringPresent);
                                    responseName?.Dispose();
                                    responseState?.Dispose();
                                    responsePresent?.Dispose();
                                    connection = Connection.OK;
                                }
                                else
                                {
                                    Logger.Instance.Debug("Could not load page. Error: {0} - {1} - {2}",
                                        responseName?.StatusCode, responseState?.StatusCode, responsePresent?.StatusCode);
                                    responseName?.Dispose();
                                    responseState?.Dispose();
                                    responsePresent?.Dispose();

                                    connection = Connection.UNREACHABLE;
                                }
                            }
                            catch (HttpRequestException)
                            {
                                Logger.Instance.Debug("Could not load uri. Cannot proceed.");

                                connection = Connection.UNREACHABLE;
                            }
                            catch (TaskCanceledException)
                            {
                                Logger.Instance.Debug("Http GET timed out.");

                                connection = Connection.TIMEOUT;
                            }
                            catch (InvalidOperationException)
                            {
                                Logger.Instance.Debug("URI was not ok.");
                                connection = Connection.UNREACHABLE;
                            }

                            if (connection == Connection.OK)
                            {
                                device.Name = responseStringName;
                                device.Present = responsePresent.Equals("1");
                                device.SwitchedOn = responseState.Equals("1");

                                customer.DeviceList.Add(device);
                            }
                            else
                            {
                                Logger.Instance.Debug("Connection not ok. Breaking from switch loop");
                                break;
                            }
                        }

                        if (connection == Connection.OK)
                        {
                            // Update time
                            customer.LastUpdatedDevices = DateTime.UtcNow;
                        }
                    }

                    // Save data in db
                    customer.LastConnectionState = connection;
                    await DynamoDB.Instance.SaveCustomerAsync(customer).ConfigureAwait(false);

                    return connection;
                }
        */
        // Handle switch turn on request
        public async Task<ConnectionState> HandleTurnOnAsync(Device device, Customer customer, bool noUpdate = false)
        {
            Connection connection = customer.GetConnection(device.ConnectionNumber);

            // Fast fail if login is blocked
            if (connection.BlockedUntil > DateTime.UtcNow)
            {
                Logger.Instance.Debug("Login still blocked. Failing.");
                return ConnectionState.BLOCKED;
            }

            Logger.Instance.Debug("Switching on device: {0}", device.Identifier);

            string fritzBoxAddress = connection.FritzBoxUrl;
            Uri uri = GetIpAddress(connection);
            if (uri != null)
            {
                fritzBoxAddress = uri.ToString();
            }

            HttpResponseMessage response = null;
            bool skipFirstTry = false;
            ConnectionState connectionState = ConnectionState.UNKNOWN;

            // if time of last update is longer than the session time, go to login immediately
            if (connection.LastUpdatedSessionId.AddMinutes(UPDATETIMESESSIONID) < DateTime.UtcNow)
            {
                skipFirstTry = true;
                Logger.Instance.Debug("Session outdated, skip first try");
            }

            // Try to set device
            try
            {
                if (!skipFirstTry)
                {
                    string requestUri;
                    if (device.IsDeviceClass(DeviceClass.Light))
                    {
                        requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=setsimpleonoff&onoff=1&ain=" + device.Identifier;
                    } else
                    {
                        requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=setswitchon&ain=" + device.Identifier;
                    }
                    if (connection.UseIPv6)
                    {
                        response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                    }
                    else
                    {
                        response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                    }
                }
            }
            catch (HttpRequestException)
            {
                Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                connectionState = ConnectionState.UNREACHABLE;
            }
            catch (TaskCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                connectionState = ConnectionState.TIMEOUT;
            }
            catch (OperationCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                connectionState = ConnectionState.TIMEOUT;
            }
            catch (InvalidOperationException)
            {
                Logger.Instance.Debug("URI was not ok.");
                connectionState = ConnectionState.UNREACHABLE;
            }

            // If retrieving was successfull, get response
            string responseString = string.Empty;
            if (response?.StatusCode == HttpStatusCode.OK)
            {
                connection.LastUpdatedSessionId = DateTime.UtcNow;
                connectionState = ConnectionState.OK;
                try
                {
                    responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                }
                catch (InvalidOperationException)
                {
                    Logger.Instance.Debug(".net core bug occured");
                    connectionState = ConnectionState.UNREACHABLE;
                }

                Logger.Instance.Debug("Got response: {0}", responseString);
                response?.Dispose();
            }
            else if (skipFirstTry || response?.StatusCode == HttpStatusCode.Forbidden)
            {
                // Forbidden: User not logged in
                Logger.Instance.Debug("Received status code FORBIDDEN. Try Login first");

                // Try to log in
                ConnectionState login = await LoginAsync(connection).ConfigureAwait(false);

                // If login was ok, try to set again
                if (login == ConnectionState.OK)
                {
                    try
                    {
                        string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=setswitchon&ain=" + device.Identifier;
                        if (connection.UseIPv6)
                        {
                            response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                        }
                        else
                        {
                            response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                        }

                        if (response?.StatusCode == HttpStatusCode.OK)
                        {
                            connectionState = ConnectionState.OK;
                            try
                            {
                                responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            }
                            catch (InvalidOperationException)
                            {
                                Logger.Instance.Debug(".net core bug occured");
                                connectionState = ConnectionState.UNREACHABLE;
                            }

                            Logger.Instance.Debug("Got response: {0}", responseString);
                            response?.Dispose();
                        }
                        else
                        {
                            Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                            response?.Dispose();
                            connectionState = ConnectionState.UNREACHABLE;
                        }
                    }
                    catch (HttpRequestException)
                    {
                        Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                        connectionState = ConnectionState.UNREACHABLE;
                    }
                    catch (TaskCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");
                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (OperationCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");
                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (InvalidOperationException)
                    {
                        Logger.Instance.Debug("URI was not ok.");
                        connectionState = ConnectionState.UNREACHABLE;
                    }
                }
                else
                {
                    Logger.Instance.Debug("Could not log in.");
                    response?.Dispose();
                    connectionState = login;
                }
            }
            else
            {
                Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                response?.Dispose();
                if (connectionState != ConnectionState.TIMEOUT)
                {
                    connectionState = ConnectionState.UNREACHABLE;
                }
            }

            if (connectionState == ConnectionState.OK)
            {
                connectionState = (responseString.Trim() == "1") ? ConnectionState.OK : ConnectionState.FAILED;
            }

            // If connection to Fritz!Box is OK, try to update device list
            if ((connectionState == ConnectionState.OK || connectionState == ConnectionState.FAILED) && !noUpdate)
            {
                connectionState = await GetDevicesAsync(customer, customer.GetConnection(device.ConnectionNumber), false, false).ConfigureAwait(false);
            }

            // if all is ok, update cache with new value
            if (connectionState == ConnectionState.OK)
            {
                Logger.Instance.Debug("Update cache.");

                // Get device
                device = customer.DeviceList.Find(listedDevice => listedDevice.Identifier == device.Identifier);

                // Update device if found
                if (device != null)
                {
                    customer.DeviceList.Remove(device);
                    device.SwitchedOn = true;
                    customer.DeviceList.Add(device);

                    // Add custom metric for datadog logging
                    Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:turnon,device:turnon",
                        DateTimeOffset.Now.ToUnixTimeSeconds(), 1, connection.UseIPv6);

                    // If item is group, update all members
                    if (device.IsGroup)
                    {
                        foreach (string member in device.GroupMembers)
                        {
                            Device deviceItem = customer.DeviceList.Single(singleDevice => singleDevice.Id == member);
                            if (deviceItem != null)
                            {
                                customer.DeviceList.Remove(deviceItem);
                                deviceItem.SwitchedOn = true;
                                customer.DeviceList.Add(deviceItem);
                            }
                        }
                    }
                }
            }
            // Connection failed, set device to not present
            else if (connectionState == ConnectionState.FAILED)
            {
                customer.DeviceList.Remove(device);
                device.Present = false;
                customer.DeviceList.Add(device);
            }

            connection.LastConnectionState = connectionState;

            // Update database
            if (!noUpdate)
            {
                PostgreSQL.Instance.SaveCustomer(customer);
            }

            return connectionState;
        }

        // Handle switch turn off request
        public async Task<ConnectionState> HandleTurnOffAsync(Device device, Customer customer, bool noUpdate = false)
        {
            Connection connection = customer.GetConnection(device.ConnectionNumber);

            // Fast fail if login is blocked
            if (connection.BlockedUntil > DateTime.UtcNow)
            {
                Logger.Instance.Debug("Login still blocked. Failing.");
                return ConnectionState.BLOCKED;
            }

            Logger.Instance.Debug("Switching off device: {0}", device.Identifier);

            string fritzBoxAddress = connection.FritzBoxUrl;
            Uri uri = GetIpAddress(connection);
            if (uri != null)
            {
                fritzBoxAddress = uri.ToString();
            }

            HttpResponseMessage response = null;
            bool skipFirstTry = false;
            ConnectionState connectionState = ConnectionState.UNKNOWN;

            // if time of last update is longer than the session time, go to login immediately
            if (connection.LastUpdatedSessionId.AddMinutes(UPDATETIMESESSIONID) < DateTime.UtcNow)
            {
                skipFirstTry = true;
                Logger.Instance.Debug("Session outdated, skip first try");
            }

            // Try to set device
            try
            {
                if (!skipFirstTry)
                {
                    string requestUri;
                    if (device.IsDeviceClass(DeviceClass.Light))
                    {
                        requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=setsimpleonoff&onoff=0&ain=" + device.Identifier;
                    }
                    else
                    {
                        requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=setswitchoff&ain=" + device.Identifier;
                    }
                    if (connection.UseIPv6)
                    {
                        response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                    }
                    else
                    {
                        response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                    }
                }
            }
            catch (HttpRequestException)
            {
                Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                connectionState = ConnectionState.UNREACHABLE;
            }
            catch (TaskCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                connectionState = ConnectionState.TIMEOUT;
            }
            catch (OperationCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                connectionState = ConnectionState.TIMEOUT;
            }
            catch (InvalidOperationException)
            {
                Logger.Instance.Debug("URI was not ok.");
                connectionState = ConnectionState.UNREACHABLE;
            }

            // If retrieving was successfull, get response
            string responseString = string.Empty;
            if (response?.StatusCode == HttpStatusCode.OK)
            {
                connection.LastUpdatedSessionId = DateTime.UtcNow;
                connectionState = ConnectionState.OK;
                try
                {
                    responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                }
                catch (InvalidOperationException)
                {
                    Logger.Instance.Debug(".net core bug occured");
                    connectionState = ConnectionState.UNREACHABLE;
                }

                Logger.Instance.Debug("Got response: {0}", responseString);
                response?.Dispose();
            }
            else if (skipFirstTry || response?.StatusCode == HttpStatusCode.Forbidden)
            {
                // Forbidden: User not logged in
                Logger.Instance.Debug("Received status code FORBIDDEN. Try Login first");

                // Try to log in
                ConnectionState login = await LoginAsync(connection).ConfigureAwait(false);

                // If login was ok, try to set again
                if (login == ConnectionState.OK)
                {
                    try
                    {
                        string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=setswitchoff&ain=" + device.Identifier;
                        if (connection.UseIPv6)
                        {
                            response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                        }
                        else
                        {
                            response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                        }

                        if (response?.StatusCode == HttpStatusCode.OK)
                        {
                            connectionState = ConnectionState.OK;
                            try
                            {
                                responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            }
                            catch (InvalidOperationException)
                            {
                                Logger.Instance.Debug(".net core bug occured");
                                connectionState = ConnectionState.UNREACHABLE;
                            }

                            Logger.Instance.Debug("Got response: {0}", responseString);
                            response?.Dispose();
                        }
                        else
                        {
                            Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                            response?.Dispose();
                            connectionState = ConnectionState.UNREACHABLE;
                        }
                    }
                    catch (HttpRequestException)
                    {
                        Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                        connectionState = ConnectionState.UNREACHABLE;
                    }
                    catch (TaskCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");
                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (OperationCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");
                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (InvalidOperationException)
                    {
                        Logger.Instance.Debug("URI was not ok.");
                        connectionState = ConnectionState.UNREACHABLE;
                    }
                }
                else
                {
                    Logger.Instance.Debug("Could not log in.");
                    response?.Dispose();
                    connectionState = login;
                }
            }
            else
            {
                Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                response?.Dispose();
                if (connectionState != ConnectionState.TIMEOUT)
                {
                    connectionState = ConnectionState.UNREACHABLE;
                }
            }

            if (connectionState == ConnectionState.OK)
            {
                connectionState = responseString.Trim() == "0" ? ConnectionState.OK : ConnectionState.FAILED;
            }

            // If connection to Fritz!Box is OK, try to update device list
            if ((connectionState == ConnectionState.OK || connectionState == ConnectionState.FAILED) && !noUpdate)
            {
                connectionState = await GetDevicesAsync(customer, customer.GetConnection(device.ConnectionNumber), false, false).ConfigureAwait(false);
            }

            // if all is ok, update cache with new value
            if (connectionState == ConnectionState.OK)
            {
                Logger.Instance.Debug("Update cache.");

                // Get device
                device = customer.DeviceList.Find(listedDevice => listedDevice.Identifier == device.Identifier);

                // Update device if found
                if (device != null)
                {
                    customer.DeviceList.Remove(device);
                    device.SwitchedOn = false;
                    customer.DeviceList.Add(device);

                    // Add custom metric for datadog logging
                    Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:turnoff,device:turnoff",
                        DateTimeOffset.Now.ToUnixTimeSeconds(), 1, connection.UseIPv6);

                    // If item is group, update all members
                    if (device.IsGroup)
                    {
                        foreach (string member in device.GroupMembers)
                        {
                            Device deviceItem = customer.DeviceList.Single(singleDevice => singleDevice.Id == member);
                            if (deviceItem != null)
                            {
                                customer.DeviceList.Remove(deviceItem);
                                deviceItem.SwitchedOn = false;
                                deviceItem.Present = true;
                                customer.DeviceList.Add(deviceItem);
                            }
                        }
                    }
                }
            }
            // Connection failed, set device to not present
            else if (connectionState == ConnectionState.FAILED)
            {
                customer.DeviceList.Remove(device);
                device.Present = false;
                customer.DeviceList.Add(device);
            }

            connection.LastConnectionState = connectionState;

            // Update database
            if (!noUpdate)
            {
                PostgreSQL.Instance.SaveCustomer(customer);
            }

            return connectionState;
        }

        // Handle set temperature request
        public async Task<ConnectionState> HandleSetTemperatureAsync(Device device, double temperature, Customer customer, bool noUpdate = false)
        {
            Connection connection = customer.GetConnection(device.ConnectionNumber);

            // Fast fail if login is blocked
            if (connection.BlockedUntil > DateTime.UtcNow)
            {
                Logger.Instance.Debug("Login still blocked. Failing.");
                return ConnectionState.BLOCKED;
            }

            int temperatureSetpoint;
            if (temperature == (double)Temperature.OFF || temperature == (double)Temperature.ON)
            {
                temperatureSetpoint = (int)temperature;
            }
            else
            {
                temperatureSetpoint = (int)(temperature * 2.0);
            }

            Logger.Instance.Debug("Switching device: {0} to {1}", device.Identifier, temperatureSetpoint);

            string fritzBoxAddress = connection.FritzBoxUrl;
            Uri uri = GetIpAddress(connection);
            if (uri != null)
            {
                fritzBoxAddress = uri.ToString();
            }

            HttpResponseMessage response = null;
            bool skipFirstTry = false;
            ConnectionState connectionState = ConnectionState.UNKNOWN;

            // if time of last update is longer than the session time, go to login immediately
            if (connection.LastUpdatedSessionId.AddMinutes(UPDATETIMESESSIONID) < DateTime.UtcNow)
            {
                skipFirstTry = true;
                Logger.Instance.Debug("Session outdated, skip first try");
            }

            // Try to set device
            try
            {
                if (!skipFirstTry)
                {
                    Logger.Instance.Debug("Trying to get page");
                    string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=sethkrtsoll&ain=" + device.Identifier + "&param=" + temperatureSetpoint;
                    if (connection.UseIPv6)
                    {
                        response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                    }
                    else
                    {
                        response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                    }
                }
            }
            catch (HttpRequestException)
            {
                Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                connectionState = ConnectionState.UNREACHABLE;
            }
            catch (TaskCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                connectionState = ConnectionState.TIMEOUT;
            }
            catch (OperationCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                connectionState = ConnectionState.TIMEOUT;
            }
            catch (InvalidOperationException)
            {
                Logger.Instance.Debug("URI was not ok.");
                connectionState = ConnectionState.UNREACHABLE;
            }

            // If retrieving was successfull, get response
            string responseString = string.Empty;
            if (response?.StatusCode == HttpStatusCode.OK)
            {
                connection.LastUpdatedSessionId = DateTime.UtcNow;
                connectionState = ConnectionState.OK;
                try
                {
                    responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                }
                catch (InvalidOperationException)
                {
                    Logger.Instance.Debug(".net core bug occured");
                    connectionState = ConnectionState.UNREACHABLE;
                }

                Logger.Instance.Debug("Got response: {0}", responseString);
                response?.Dispose();
            }
            else if (skipFirstTry || response?.StatusCode == HttpStatusCode.Forbidden)
            {
                // Forbidden: User not logged in
                Logger.Instance.Debug("Received status code FORBIDDEN. Try Login first");

                // Try to log in
                ConnectionState login = await LoginAsync(connection).ConfigureAwait(false);

                // If login was ok, try to set again
                if (login == ConnectionState.OK)
                {
                    try
                    {
                        string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=sethkrtsoll&ain=" + device.Identifier + "&param=" + temperatureSetpoint;
                        if (connection.UseIPv6)
                        {
                            response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                        }
                        else
                        {
                            response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                        }

                        if (response?.StatusCode == HttpStatusCode.OK)
                        {
                            connectionState = ConnectionState.OK;
                            try
                            {
                                responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            }
                            catch (InvalidOperationException)
                            {
                                Logger.Instance.Debug(".net core bug occured");
                                connectionState = ConnectionState.UNREACHABLE;
                            }

                            Logger.Instance.Debug("Got response: {0}", responseString);
                            response?.Dispose();
                        }
                        else
                        {
                            Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                            response?.Dispose();
                            connectionState = ConnectionState.UNREACHABLE;
                        }
                    }
                    catch (HttpRequestException)
                    {
                        Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                        connectionState = ConnectionState.UNREACHABLE;
                    }
                    catch (TaskCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");
                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (OperationCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");
                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (InvalidOperationException)
                    {
                        Logger.Instance.Debug("URI was not ok.");
                    }
                }
                else
                {
                    Logger.Instance.Debug("Could not log in.");
                    response?.Dispose();
                    connectionState = login;
                }
            }
            else
            {
                Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                response?.Dispose();
                if (connectionState != ConnectionState.TIMEOUT)
                {
                    connectionState = ConnectionState.UNREACHABLE;
                }
            }

            if (connectionState == ConnectionState.OK)
            {
                connectionState = responseString.Trim() == temperatureSetpoint.ToString().Trim() ? ConnectionState.OK : ConnectionState.FAILED;
            }

            // If connection to Fritz!Box is OK, try to update device list
            if ((connectionState == ConnectionState.OK || connectionState == ConnectionState.FAILED) && !noUpdate)
            {
                connectionState = await GetDevicesAsync(customer, customer.GetConnection(device.ConnectionNumber), false, false).ConfigureAwait(false);
            }

            // if all is ok, update cache with new setpoint value
            if (connectionState == ConnectionState.OK)
            {
                // Get device
                device = customer.DeviceList.Find(listedDevice => listedDevice.Identifier == device.Identifier);

                Logger.Instance.Debug("Update cache.");

                // Update device if found 
                if (device != null)
                {
                    customer.DeviceList.Remove(device);
                    device.TemperatureSetpoint = temperature;
                    customer.DeviceList.Add(device);

                    // Add custom metric for datadog logging
                    Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:settemperature,device:settemperature",
                        DateTimeOffset.Now.ToUnixTimeSeconds(), 1, connection.UseIPv6);

                    // If item is group, update all members
                    if (device.IsGroup)
                    {
                        foreach (string member in device.GroupMembers)
                        {
                            Device deviceItem = customer.DeviceList.Single(singleDevice => singleDevice.Id == member);
                            if (deviceItem != null)
                            {
                                customer.DeviceList.Remove(deviceItem);
                                deviceItem.TemperatureSetpoint = temperature;
                                deviceItem.Present = true;
                                customer.DeviceList.Add(deviceItem);
                            }
                        }
                    }
                }
            }
            // Connection failed, set device to not present
            else if (connectionState == ConnectionState.FAILED)
            {
                customer.DeviceList.Remove(device);
                device.Present = false;
                customer.DeviceList.Add(device);
            }

            connection.LastConnectionState = connectionState;

            // Update database
            if (!noUpdate)
            {
                PostgreSQL.Instance.SaveCustomer(customer);
            }

            return connectionState;
        }

        // Handle apply template request
        public async Task<ConnectionState> HandleApplyTemplateAsync(Device device, Customer customer, bool noUpdate = false)
        {
            Connection connection = customer.GetConnection(device.ConnectionNumber);

            // Fast fail if login is blocked
            if (connection.BlockedUntil > DateTime.UtcNow)
            {
                Logger.Instance.Debug("Login still blocked. Failing.");
                return ConnectionState.BLOCKED;
            }

            Logger.Instance.Debug("Switching on template: {0}", device.Identifier);

            string fritzBoxAddress = connection.FritzBoxUrl;
            Uri uri = GetIpAddress(connection);
            if (uri != null)
            {
                fritzBoxAddress = uri.ToString();
            }

            HttpResponseMessage response = null;
            bool skipFirstTry = false;
            ConnectionState connectionState = ConnectionState.UNKNOWN;

            // if time of last update is longer than the session time, go to login immediately
            if (connection.LastUpdatedSessionId.AddMinutes(UPDATETIMESESSIONID) < DateTime.UtcNow)
            {
                skipFirstTry = true;
                Logger.Instance.Debug("Session outdated, skip first try");
            }

            // Try to set device
            try
            {
                if (!skipFirstTry)
                {
                    string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=applytemplate&ain=" + device.Identifier;
                    if (connection.UseIPv6)
                    {
                        response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                    }
                    else
                    {
                        response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                    }
                }
            }
            catch (HttpRequestException)
            {
                Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                connectionState = ConnectionState.UNREACHABLE;
            }
            catch (TaskCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                connectionState = ConnectionState.TIMEOUT;
            }
            catch (OperationCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                connectionState = ConnectionState.TIMEOUT;
            }
            catch (InvalidOperationException)
            {
                Logger.Instance.Debug("URI was not ok.");
                connectionState = ConnectionState.UNREACHABLE;
            }

            // If retrieving was successfull, get response
            string responseString = string.Empty;
            if (response?.StatusCode == HttpStatusCode.OK)
            {
                connection.LastUpdatedSessionId = DateTime.UtcNow;
                connectionState = ConnectionState.OK;
                try
                {
                    responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                }
                catch (InvalidOperationException)
                {
                    Logger.Instance.Debug(".net core bug occured");
                    connectionState = ConnectionState.UNREACHABLE;
                }

                Logger.Instance.Debug("Got response: {0}", responseString);
                response?.Dispose();
            }
            else if (skipFirstTry || response?.StatusCode == HttpStatusCode.Forbidden)
            {
                // Forbidden: User not logged in
                Logger.Instance.Debug("Received status code FORBIDDEN. Try Login first");

                // Try to log in
                ConnectionState login = await LoginAsync(connection).ConfigureAwait(false);

                // If login was ok, try to set again
                if (login == ConnectionState.OK)
                {
                    try
                    {
                        string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=applytemplate&ain=" + device.Identifier;
                        if (connection.UseIPv6)
                        {
                            response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                        }
                        else
                        {
                            response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                        }

                        if (response?.StatusCode == HttpStatusCode.OK)
                        {
                            connectionState = ConnectionState.OK;
                            try
                            {
                                responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            }
                            catch (InvalidOperationException)
                            {
                                Logger.Instance.Debug(".net core bug occured");
                                connectionState = ConnectionState.UNREACHABLE;
                            }

                            Logger.Instance.Debug("Got response: {0}", responseString);
                            response?.Dispose();
                        }
                        else
                        {
                            Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                            response?.Dispose();
                            connectionState = ConnectionState.UNREACHABLE;
                        }
                    }
                    catch (HttpRequestException)
                    {
                        Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                        connectionState = ConnectionState.UNREACHABLE;
                    }
                    catch (TaskCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");
                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (OperationCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");
                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (InvalidOperationException)
                    {
                        Logger.Instance.Debug("URI was not ok.");
                        connectionState = ConnectionState.UNREACHABLE;
                    }
                }
                else
                {
                    Logger.Instance.Debug("Could not log in.");
                    response?.Dispose();
                    connectionState = login;
                }
            }
            else
            {
                Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                response?.Dispose();
                if (connectionState != ConnectionState.TIMEOUT)
                {
                    connectionState = ConnectionState.UNREACHABLE;
                }
            }

            if (connectionState == ConnectionState.OK)
            {
                connectionState = (responseString.Trim() == device.Id.Trim()) ? ConnectionState.OK : ConnectionState.FAILED;
            }

            // If connection to Fritz!Box is OK, try to update device list
            if ((connectionState == ConnectionState.OK || connectionState == ConnectionState.FAILED) && !noUpdate)
            {
                connectionState = await GetDevicesAsync(customer, customer.GetConnection(device.ConnectionNumber), false, false).ConfigureAwait(false);
            }

            // if all is ok, add logging
            if (connectionState == ConnectionState.OK)
            {
                Logger.Instance.Debug("Update cache.");

                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:applytemplate,device:applytemplate",
                    DateTimeOffset.Now.ToUnixTimeSeconds(), 1, connection.UseIPv6);
            }

            connection.LastConnectionState = connectionState;

            // Update database
            if (!noUpdate)
            {
                PostgreSQL.Instance.SaveCustomer(customer);
            }

            return connectionState;
        }

        // Handle multiple requests
        public async Task<Dictionary<string, ConnectionState>> HandleMultipleCommandsAsync(Dictionary<string, object> commandList, Customer customer)
        {
            // Create response list
            Dictionary<string, ConnectionState> response = new Dictionary<string, ConnectionState>();

            // Create list of already updated connections
            Dictionary<ConnectionNumber, ConnectionState> updatedConnections = new Dictionary<ConnectionNumber, ConnectionState>();

            foreach (KeyValuePair<string, object> command in commandList)
            {
                Logger.Instance.Debug("Device: {0}, Command: {1}", command.Key, command.Value);

                ConnectionState connectionState = ConnectionState.UNKNOWN;

                // Get device
                Device deviceItem = customer.DeviceList.Find(device => device.Identifier == command.Key.Replace("-virtual", ""));
                if (deviceItem == null)
                {
                    // Device not found, continue with next device
                    response.Add(command.Key, ConnectionState.UNKNOWN);
                    continue;
                }

                // Get connection
                Connection connection = customer.GetConnection(deviceItem.ConnectionNumber);

                // Check connection and update device list. Only update if not updated yet
                if (!updatedConnections.ContainsKey(connection.Number))
                {
                    connectionState = await GetDevicesAsync(customer, customer.GetConnection(connection.Number), false, false).ConfigureAwait(false);
                    updatedConnections.Add(connection.Number, connectionState);
                }

                // If connection is known to be not ok, update return list and continue
                if (updatedConnections.GetValueOrDefault(connection.Number) != ConnectionState.OK)
                {
                    response.Add(command.Key, connectionState);
                    continue;
                }

                connectionState = ConnectionState.UNKNOWN;

                // Check for on/off or set temperature command
                if (command.Value is bool)
                {
                    // If device is a thermostat, hand over to thermostat control
                    if (deviceItem.IsDeviceClass(DeviceClass.Thermostat))
                    {
                        double onTemperature = deviceItem.TemperatureComfort;
                        double offTemperature = (double)Temperature.OFF;

                        // check for user set temperature. If not set, use comfort temperature
                        if ((deviceItem.TemperatureOn >= (double)Temperature.MINIMUM_VALUE && deviceItem.TemperatureOn <= (double)Temperature.MAXIMUM_VALUE)
                            || (deviceItem.TemperatureOn >= (double)Temperature.NOCHANGE && deviceItem.TemperatureOn <= (double)Temperature.COMFORT))
                        {
                            if (deviceItem.TemperatureOn == (double)Temperature.NOCHANGE)
                            {
                                onTemperature = deviceItem.TemperatureSetpoint;
                            }
                            else if (deviceItem.TemperatureOn == (double)Temperature.ECO)
                            {
                                onTemperature = deviceItem.TemperatureSetback;
                            }
                            else if (deviceItem.TemperatureOn == (double)Temperature.COMFORT)
                            {
                                onTemperature = deviceItem.TemperatureComfort;
                            }
                            else
                            {
                                onTemperature = deviceItem.TemperatureOn;
                            }
                        }

                        // check for user set temperature. If not set, set thermostat off
                        if ((deviceItem.TemperatureOff >= (double)Temperature.MINIMUM_VALUE && deviceItem.TemperatureOff <= (double)Temperature.MAXIMUM_VALUE)
                            || (deviceItem.TemperatureOff >= (double)Temperature.NOCHANGE && deviceItem.TemperatureOff <= (double)Temperature.COMFORT))
                        {
                            if (deviceItem.TemperatureOff == (double)Temperature.NOCHANGE)
                            {
                                offTemperature = deviceItem.TemperatureSetpoint;
                            }
                            else if (deviceItem.TemperatureOff == (double)Temperature.ECO)
                            {
                                offTemperature = deviceItem.TemperatureSetback;
                            }
                            else if (deviceItem.TemperatureOff == (double)Temperature.COMFORT)
                            {
                                offTemperature = deviceItem.TemperatureComfort;
                            }
                            else
                            {
                                offTemperature = deviceItem.TemperatureOff;
                            }
                        }

                        double temperature;
                        if ((bool)command.Value)
                        {
                            temperature = onTemperature;
                        }
                        else
                        {
                            temperature = offTemperature;
                        }

                        // If no change is requested, just update response
                        if ((bool)command.Value && deviceItem.TemperatureOn == (double)Temperature.NOCHANGE)
                        {
                            connectionState = connection.LastConnectionState;
                        }
                        else if (!(bool)command.Value && deviceItem.TemperatureOff == (double)Temperature.NOCHANGE)
                        {
                            connectionState = connection.LastConnectionState;
                        }
                        else
                        {
                            // Else update temperature
                            connectionState = await HandleSetTemperatureAsync(deviceItem, temperature, customer, true).ConfigureAwait(false);
                        }
                        response.Add(command.Key, connectionState);
                    }
                    // device is template
                    else if (deviceItem.IsDeviceClass(DeviceClass.Template))
                    {
                        // Handle apply template command
                        connectionState = await HandleApplyTemplateAsync(deviceItem, customer, true).ConfigureAwait(false);
                        response.Add(command.Key, connectionState);
                    }
                    else
                    {
                        // device is outlet or light
                        if ((bool)command.Value)
                        {
                            // Handle turn on command
                            connectionState = await HandleTurnOnAsync(deviceItem, customer, true).ConfigureAwait(false);
                            response.Add(command.Key, connectionState);
                        }
                        else
                        {
                            // Handle turn off command
                            connectionState = await HandleTurnOffAsync(deviceItem, customer, true).ConfigureAwait(false);
                            response.Add(command.Key, connectionState);
                        }
                    }
                }
                else if (command.Value is double)
                {
                    // device is thermostat
                    if (deviceItem.IsDeviceClass(DeviceClass.Thermostat))
                    {
                        connectionState = await HandleSetTemperatureAsync(deviceItem, (double)command.Value, customer, true).ConfigureAwait(false);
                        response.Add(command.Key, connectionState);
                    }
                    // device is light
                    else
                    {
                        connectionState = await HandleSetLevelAsync(deviceItem, (double)command.Value, customer, true).ConfigureAwait(false);
                        response.Add(command.Key, connectionState);
                    }

                }
                else if (command.Value is string)
                {
                    string commandValue = (string)command.Value;

                    // If command ends with 'K' it is to set light color temperature
                    if (commandValue.EndsWith("K"))
                    {
                        commandValue = commandValue.TrimEnd('K');
                        connectionState = await HandleSetLightColorTemperatureAsync(deviceItem, double.Parse(commandValue), customer, true).ConfigureAwait(false);
                        response.Add(command.Key, connectionState);
                    }
                    // If command includes '/' it is to set light color
                    else if (commandValue.Contains('/'))
                    {
                        String[] split = commandValue.Split(new char[] { '/' });
                        double hue = double.Parse(split[0]);
                        double saturation = double.Parse(split[1]);
                        connectionState = await HandleSetLightColorAsync(deviceItem, (double)hue, (double)saturation, customer, true).ConfigureAwait(false);
                        response.Add(command.Key, connectionState);
                    }

                }
                else
                {
                    response.Add(command.Key, ConnectionState.UNKNOWN);
                }
            }

            // Update connections
            foreach (KeyValuePair<string, ConnectionState> responseItem in response)
            {
                Device deviceItem = customer.DeviceList.Find(device => device.Identifier == responseItem.Key.Replace("-virtual", ""));
                if (deviceItem != null)
                {
                    Connection connection = customer.GetConnection(deviceItem.ConnectionNumber);
                    connection.LastConnectionState = responseItem.Value;
                }
            }

            // Update customer
            PostgreSQL.Instance.SaveCustomer(customer);

            return response;
        }

        // Handle set level request
        public async Task<ConnectionState> HandleSetLevelAsync(Device device, double level, Customer customer, bool noUpdate = false)
        {
            int levelSetpoint = (int)(level / 100.0 * 255.0);
            if (levelSetpoint < 0) 
            {
                levelSetpoint = 0;
            }
            else if(levelSetpoint > 255) 
            {
                levelSetpoint = 255;
            }

            Connection connection = customer.GetConnection(device.ConnectionNumber);

            // Fast fail if login is blocked
            if (connection.BlockedUntil > DateTime.UtcNow)
            {
                Logger.Instance.Debug("Login still blocked. Failing.");
                return ConnectionState.BLOCKED;
            }

            Logger.Instance.Debug("Switching level on device: {0}", device.Identifier);

            string fritzBoxAddress = connection.FritzBoxUrl;
            Uri uri = GetIpAddress(connection);
            if (uri != null)
            {
                fritzBoxAddress = uri.ToString();
            }

            HttpResponseMessage response = null;
            bool skipFirstTry = false;
            ConnectionState connectionState = ConnectionState.UNKNOWN;

            // if time of last update is longer than the session time, go to login immediately
            if (connection.LastUpdatedSessionId.AddMinutes(UPDATETIMESESSIONID) < DateTime.UtcNow)
            {
                skipFirstTry = true;
                Logger.Instance.Debug("Session outdated, skip first try");
            }

            // Try to set device
            try
            {
                if (!skipFirstTry)
                {
                    string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=setlevel&level=" + levelSetpoint + "&ain=" + device.Identifier;

                    if (connection.UseIPv6)
                    {
                        response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                    }
                    else
                    {
                        response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                    }
                }
            }
            catch (HttpRequestException)
            {
                Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                connectionState = ConnectionState.UNREACHABLE;
            }
            catch (TaskCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                connectionState = ConnectionState.TIMEOUT;
            }
            catch (OperationCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                connectionState = ConnectionState.TIMEOUT;
            }
            catch (InvalidOperationException)
            {
                Logger.Instance.Debug("URI was not ok.");
                connectionState = ConnectionState.UNREACHABLE;
            }

            // If retrieving was successfull, get response
            string responseString = string.Empty;
            if (response?.StatusCode == HttpStatusCode.OK)
            {
                connection.LastUpdatedSessionId = DateTime.UtcNow;
                connectionState = ConnectionState.OK;
                try
                {
                    responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                }
                catch (InvalidOperationException)
                {
                    Logger.Instance.Debug(".net core bug occured");
                    connectionState = ConnectionState.UNREACHABLE;
                }

                Logger.Instance.Debug("Got response: {0}", responseString);
                response?.Dispose();
            }
            else if (skipFirstTry || response?.StatusCode == HttpStatusCode.Forbidden)
            {
                // Forbidden: User not logged in
                Logger.Instance.Debug("Received status code FORBIDDEN. Try Login first");

                // Try to log in
                ConnectionState login = await LoginAsync(connection).ConfigureAwait(false);

                // If login was ok, try to set again
                if (login == ConnectionState.OK)
                {
                    try
                    {
                        string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=setlevel&level=" + levelSetpoint + "&ain=" + device.Identifier;
                        if (connection.UseIPv6)
                        {
                            response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                        }
                        else
                        {
                            response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                        }

                        if (response?.StatusCode == HttpStatusCode.OK)
                        {
                            connectionState = ConnectionState.OK;
                            try
                            {
                                responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            }
                            catch (InvalidOperationException)
                            {
                                Logger.Instance.Debug(".net core bug occured");
                                connectionState = ConnectionState.UNREACHABLE;
                            }

                            Logger.Instance.Debug("Got response: {0}", responseString);
                            response?.Dispose();
                        }
                        else
                        {
                            Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                            response?.Dispose();
                            connectionState = ConnectionState.UNREACHABLE;
                        }
                    }
                    catch (HttpRequestException)
                    {
                        Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                        connectionState = ConnectionState.UNREACHABLE;
                    }
                    catch (TaskCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");
                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (OperationCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");
                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (InvalidOperationException)
                    {
                        Logger.Instance.Debug("URI was not ok.");
                        connectionState = ConnectionState.UNREACHABLE;
                    }
                }
                else
                {
                    Logger.Instance.Debug("Could not log in.");
                    response?.Dispose();
                    connectionState = login;
                }
            }
            else
            {
                Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                response?.Dispose();
                if (connectionState != ConnectionState.TIMEOUT)
                {
                    connectionState = ConnectionState.UNREACHABLE;
                }
            }

            if (connectionState == ConnectionState.OK)
            {
                connectionState = (responseString.Trim() == levelSetpoint.ToString()) ? ConnectionState.OK : ConnectionState.FAILED;
            }

            // If connection to Fritz!Box is OK, try to update device list
            if ((connectionState == ConnectionState.OK || connectionState == ConnectionState.FAILED) && !noUpdate)
            {
                connectionState = await GetDevicesAsync(customer, customer.GetConnection(device.ConnectionNumber), false, false).ConfigureAwait(false);
            }

            // if all is ok, update cache with new value
            if (connectionState == ConnectionState.OK)
            {
                Logger.Instance.Debug("Update cache.");

                // Get device
                device = customer.DeviceList.Find(listedDevice => listedDevice.Identifier == device.Identifier);

                // Update device if found
                if (device != null)
                {
                    customer.DeviceList.Remove(device);
                    device.Level = (int)(levelSetpoint / 255.0 * 100.0);
                    customer.DeviceList.Add(device);

                    // Add custom metric for datadog logging
                    Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:setlevel,device:setlevel",
                        DateTimeOffset.Now.ToUnixTimeSeconds(), 1, connection.UseIPv6);

                    // If item is group, update all members
                    if (device.IsGroup)
                    {
                        foreach (string member in device.GroupMembers)
                        {
                            Device deviceItem = customer.DeviceList.Single(singleDevice => singleDevice.Id == member);
                            if (deviceItem != null)
                            {
                                customer.DeviceList.Remove(deviceItem);
                                deviceItem.Level = (int)(levelSetpoint / 255.0 * 100.0);
                                customer.DeviceList.Add(deviceItem);
                            }
                        }
                    }
                }
            }
            // Connection failed, set device to not present
            else if (connectionState == ConnectionState.FAILED)
            {
                customer.DeviceList.Remove(device);
                device.Present = false;
                customer.DeviceList.Add(device);
            }

            connection.LastConnectionState = connectionState;

            // Update database
            if (!noUpdate)
            {
                PostgreSQL.Instance.SaveCustomer(customer);
            }

            return connectionState;
        }

        // Handle set light color temperature request
        public async Task<ConnectionState> HandleSetLightColorTemperatureAsync(Device device, double colorTemperature, Customer customer, bool noUpdate = false)
        {
            // Get closest color temperature from template list
            int colorTemperatureSetpoint = (int)colorTemperature;

            if(device.ColorTemperatures.Count > 0)
            {
                colorTemperatureSetpoint = (int)device.ColorTemperatures.Aggregate((x, y) => Math.Abs(x - colorTemperature) < Math.Abs(y - colorTemperature) ? x : y);
            }

            Connection connection = customer.GetConnection(device.ConnectionNumber);

            // Fast fail if login is blocked
            if (connection.BlockedUntil > DateTime.UtcNow)
            {
                Logger.Instance.Debug("Login still blocked. Failing.");
                return ConnectionState.BLOCKED;
            }

            Logger.Instance.Debug("Switching on device: {0}", device.Identifier);

            string fritzBoxAddress = connection.FritzBoxUrl;
            Uri uri = GetIpAddress(connection);
            if (uri != null)
            {
                fritzBoxAddress = uri.ToString();
            }

            HttpResponseMessage response = null;
            bool skipFirstTry = false;
            ConnectionState connectionState = ConnectionState.UNKNOWN;

            // if time of last update is longer than the session time, go to login immediately
            if (connection.LastUpdatedSessionId.AddMinutes(UPDATETIMESESSIONID) < DateTime.UtcNow)
            {
                skipFirstTry = true;
                Logger.Instance.Debug("Session outdated, skip first try");
            }

            // Try to set device
            try
            {
                if (!skipFirstTry)
                {
                    string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=setcolortemperature&duration=0&temperature=" + colorTemperatureSetpoint + "&ain=" + device.Identifier;

                    if (connection.UseIPv6)
                    {
                        response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                    }
                    else
                    {
                        response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                    }
                }
            }
            catch (HttpRequestException)
            {
                Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                connectionState = ConnectionState.UNREACHABLE;
            }
            catch (TaskCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                connectionState = ConnectionState.TIMEOUT;
            }
            catch (OperationCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                connectionState = ConnectionState.TIMEOUT;
            }
            catch (InvalidOperationException)
            {
                Logger.Instance.Debug("URI was not ok.");
                connectionState = ConnectionState.UNREACHABLE;
            }

            // If retrieving was successfull, get response
            string responseString = string.Empty;
            if (response?.StatusCode == HttpStatusCode.OK)
            {
                connection.LastUpdatedSessionId = DateTime.UtcNow;
                connectionState = ConnectionState.OK;
                try
                {
                    responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                }
                catch (InvalidOperationException)
                {
                    Logger.Instance.Debug(".net core bug occured");
                    connectionState = ConnectionState.UNREACHABLE;
                }

                Logger.Instance.Debug("Got response: {0}", responseString);
                response?.Dispose();
            }
            else if (skipFirstTry || response?.StatusCode == HttpStatusCode.Forbidden)
            {
                // Forbidden: User not logged in
                Logger.Instance.Debug("Received status code FORBIDDEN. Try Login first");

                // Try to log in
                ConnectionState login = await LoginAsync(connection).ConfigureAwait(false);

                // If login was ok, try to set again
                if (login == ConnectionState.OK)
                {
                    try
                    {
                        string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=setcolortemperature&duration=0&temperature=" + colorTemperatureSetpoint + "&ain=" + device.Identifier;
                        if (connection.UseIPv6)
                        {
                            response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                        }
                        else
                        {
                            response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                        }

                        if (response?.StatusCode == HttpStatusCode.OK)
                        {
                            connectionState = ConnectionState.OK;
                            try
                            {
                                responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            }
                            catch (InvalidOperationException)
                            {
                                Logger.Instance.Debug(".net core bug occured");
                                connectionState = ConnectionState.UNREACHABLE;
                            }

                            Logger.Instance.Debug("Got response: {0}", responseString);
                            response?.Dispose();
                        }
                        else
                        {
                            Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                            response?.Dispose();
                            connectionState = ConnectionState.UNREACHABLE;
                        }
                    }
                    catch (HttpRequestException)
                    {
                        Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                        connectionState = ConnectionState.UNREACHABLE;
                    }
                    catch (TaskCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");
                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (OperationCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");
                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (InvalidOperationException)
                    {
                        Logger.Instance.Debug("URI was not ok.");
                        connectionState = ConnectionState.UNREACHABLE;
                    }
                }
                else
                {
                    Logger.Instance.Debug("Could not log in.");
                    response?.Dispose();
                    connectionState = login;
                }
            }
            else
            {
                Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                response?.Dispose();
                if (connectionState != ConnectionState.TIMEOUT)
                {
                    connectionState = ConnectionState.UNREACHABLE;
                }
            }

            if (connectionState == ConnectionState.OK)
            {
                connectionState = (responseString.Trim() == colorTemperatureSetpoint.ToString()) ? ConnectionState.OK : ConnectionState.FAILED;
            }

            // If connection to Fritz!Box is OK, try to update device list
            if ((connectionState == ConnectionState.OK || connectionState == ConnectionState.FAILED) && !noUpdate)
            {
                connectionState = await GetDevicesAsync(customer, customer.GetConnection(device.ConnectionNumber), false, false).ConfigureAwait(false);
            }

            // if all is ok, update cache with new value
            if (connectionState == ConnectionState.OK)
            {
                Logger.Instance.Debug("Update cache.");

                // Get device
                device = customer.DeviceList.Find(listedDevice => listedDevice.Identifier == device.Identifier);

                // Update device if found
                if (device != null)
                {
                    customer.DeviceList.Remove(device);
                    device.Temperature = colorTemperature;
                    device.Hue = 0;
                    device.Saturation = 0;
                    customer.DeviceList.Add(device);

                    // Add custom metric for datadog logging
                    Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:setcolortemperature,device:setcolortemperature",
                        DateTimeOffset.Now.ToUnixTimeSeconds(), 1, connection.UseIPv6);

                    // If item is group, update all members
                    if (device.IsGroup)
                    {
                        foreach (string member in device.GroupMembers)
                        {
                            Device deviceItem = customer.DeviceList.Single(singleDevice => singleDevice.Id == member);
                            if (deviceItem != null)
                            {
                                customer.DeviceList.Remove(deviceItem);
                                deviceItem.Temperature = colorTemperature;
                                device.Hue = 0;
                                device.Saturation = 0;
                                customer.DeviceList.Add(deviceItem);
                            }
                        }
                    }
                }
            }
            // Connection failed, set device to not present
            else if (connectionState == ConnectionState.FAILED)
            {
                customer.DeviceList.Remove(device);
                device.Present = false;
                customer.DeviceList.Add(device);
            }

            connection.LastConnectionState = connectionState;

            // Update database
            if (!noUpdate)
            {
                PostgreSQL.Instance.SaveCustomer(customer);
            }

            return connectionState;
        }

        // Handle set light color request
        public async Task<ConnectionState> HandleSetLightColorAsync(Device device, double hue, double saturation, Customer customer, bool noUpdate = false)
        {
            // Get closest color from template list
            int hueSetpoint = (int)hue;
            int saturationSetpoint = (int)(saturation / 100.0 * 255.0);

            double closestHue = -1;
            double closestSaturation = -1;
            ColorTemplate foundTemplate = null;

            foreach (ColorTemplate template in device.Colors)
            {
                if(closestHue == -1 || Math.Min(Math.Abs(hueSetpoint - closestHue), 360.0 - Math.Abs(hueSetpoint - closestHue)) > 
                    Math.Min(Math.Abs(template.Hue - hueSetpoint), 360.0 - Math.Abs(template.Hue - hueSetpoint)))
                {
                    closestHue = template.Hue;
                    foundTemplate = template;
                }
            }

            if(foundTemplate != null)
            {
                hueSetpoint = (int)closestHue;

                foreach(double templateSaturation in foundTemplate.Saturation)
                {
                    if (closestSaturation == -1 || Math.Abs(saturationSetpoint - closestSaturation) > Math.Abs(templateSaturation - saturationSetpoint))
                    {
                        closestSaturation = templateSaturation;
                    }
                }
            }

            if(closestSaturation != -1)
            {
                saturationSetpoint = (int)closestSaturation;
            }

            Connection connection = customer.GetConnection(device.ConnectionNumber);

            // Fast fail if login is blocked
            if (connection.BlockedUntil > DateTime.UtcNow)
            {
                Logger.Instance.Debug("Login still blocked. Failing.");
                return ConnectionState.BLOCKED;
            }

            Logger.Instance.Debug("Switching on device: {0}", device.Identifier);

            string fritzBoxAddress = connection.FritzBoxUrl;
            Uri uri = GetIpAddress(connection);
            if (uri != null)
            {
                fritzBoxAddress = uri.ToString();
            }

            HttpResponseMessage response = null;
            bool skipFirstTry = false;
            ConnectionState connectionState = ConnectionState.UNKNOWN;

            // if time of last update is longer than the session time, go to login immediately
            if (connection.LastUpdatedSessionId.AddMinutes(UPDATETIMESESSIONID) < DateTime.UtcNow)
            {
                skipFirstTry = true;
                Logger.Instance.Debug("Session outdated, skip first try");
            }

            // Try to set device
            try
            {
                if (!skipFirstTry)
                {
                    string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=setcolor&duration=0&hue=" + hueSetpoint + "&saturation=" + saturationSetpoint + "&ain=" + device.Identifier;

                    if (connection.UseIPv6)
                    {
                        response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                    }
                    else
                    {
                        response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                    }
                }
            }
            catch (HttpRequestException)
            {
                Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                connectionState = ConnectionState.UNREACHABLE;
            }
            catch (TaskCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                connectionState = ConnectionState.TIMEOUT;
            }
            catch (OperationCanceledException)
            {
                Logger.Instance.Debug("Http GET timed out.");
                connectionState = ConnectionState.TIMEOUT;
            }
            catch (InvalidOperationException)
            {
                Logger.Instance.Debug("URI was not ok.");
                connectionState = ConnectionState.UNREACHABLE;
            }

            // If retrieving was successfull, get response
            string responseString = string.Empty;
            if (response?.StatusCode == HttpStatusCode.OK)
            {
                connection.LastUpdatedSessionId = DateTime.UtcNow;
                connectionState = ConnectionState.OK;
                try
                {
                    responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                }
                catch (InvalidOperationException)
                {
                    Logger.Instance.Debug(".net core bug occured");
                    connectionState = ConnectionState.UNREACHABLE;
                }

                Logger.Instance.Debug("Got response: {0}", responseString);
                response?.Dispose();
            }
            else if (skipFirstTry || response?.StatusCode == HttpStatusCode.Forbidden)
            {
                // Forbidden: User not logged in
                Logger.Instance.Debug("Received status code FORBIDDEN. Try Login first");

                // Try to log in
                ConnectionState login = await LoginAsync(connection).ConfigureAwait(false);

                // If login was ok, try to set again
                if (login == ConnectionState.OK)
                {
                    try
                    {
                        string requestUri = fritzBoxAddress.TrimEnd('/') + "/" + SMARTHOMEPAGE + "?sid=" + connection.FritzBoxSessionId + "&switchcmd=setcolor&duration=0&hue=" + hueSetpoint + "&saturation=" + saturationSetpoint + "&ain=" + device.Identifier;
                        if (connection.UseIPv6)
                        {
                            response = await ProxyClient.GetAsync(requestUri).ConfigureAwait(false);
                        }
                        else
                        {
                            response = await Client.GetAsync(requestUri).ConfigureAwait(false);
                        }

                        if (response?.StatusCode == HttpStatusCode.OK)
                        {
                            connectionState = ConnectionState.OK;
                            try
                            {
                                responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            }
                            catch (InvalidOperationException)
                            {
                                Logger.Instance.Debug(".net core bug occured");
                                connectionState = ConnectionState.UNREACHABLE;
                            }

                            Logger.Instance.Debug("Got response: {0}", responseString);
                            response?.Dispose();
                        }
                        else
                        {
                            Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                            response?.Dispose();
                            connectionState = ConnectionState.UNREACHABLE;
                        }
                    }
                    catch (HttpRequestException)
                    {
                        Logger.Instance.Debug("Could not load uri. Cannot proceed.");
                        connectionState = ConnectionState.UNREACHABLE;
                    }
                    catch (TaskCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");
                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (OperationCanceledException)
                    {
                        Logger.Instance.Debug("Http GET timed out.");
                        connectionState = ConnectionState.TIMEOUT;
                    }
                    catch (InvalidOperationException)
                    {
                        Logger.Instance.Debug("URI was not ok.");
                        connectionState = ConnectionState.UNREACHABLE;
                    }
                }
                else
                {
                    Logger.Instance.Debug("Could not log in.");
                    response?.Dispose();
                    connectionState = login;
                }
            }
            else
            {
                Logger.Instance.Debug("Could not load page. Error: {0}", response?.StatusCode);
                response?.Dispose();
                if (connectionState != ConnectionState.TIMEOUT)
                {
                    connectionState = ConnectionState.UNREACHABLE;
                }
            }

            if (connectionState == ConnectionState.OK)
            {
                connectionState = (responseString.Trim() == hueSetpoint.ToString()) ? ConnectionState.OK : ConnectionState.FAILED;
            }

            // If connection to Fritz!Box is OK, try to update device list
            if ((connectionState == ConnectionState.OK || connectionState == ConnectionState.FAILED) && !noUpdate)
            {
                connectionState = await GetDevicesAsync(customer, customer.GetConnection(device.ConnectionNumber), false, false).ConfigureAwait(false);
            }

            // if all is ok, update cache with new value
            if (connectionState == ConnectionState.OK)
            {
                Logger.Instance.Debug("Update cache.");

                // Get device
                device = customer.DeviceList.Find(listedDevice => listedDevice.Identifier == device.Identifier);

                // Update device if found
                if (device != null)
                {
                    customer.DeviceList.Remove(device);
                    device.Temperature = 0;
                    device.Hue = hue;
                    device.Saturation = saturation;
                    customer.DeviceList.Add(device);

                    // Add custom metric for datadog logging
                    Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:setcolor,device:setcolor",
                        DateTimeOffset.Now.ToUnixTimeSeconds(), 1, connection.UseIPv6);

                    // If item is group, update all members
                    if (device.IsGroup)
                    {
                        foreach (string member in device.GroupMembers)
                        {
                            Device deviceItem = customer.DeviceList.Single(singleDevice => singleDevice.Id == member);
                            if (deviceItem != null)
                            {
                                customer.DeviceList.Remove(deviceItem);
                                deviceItem.Temperature = 0;
                                device.Hue = hue;
                                device.Saturation = saturation;
                                customer.DeviceList.Add(deviceItem);
                            }
                        }
                    }
                }
            }
            // Connection failed, set device to not present
            else if (connectionState == ConnectionState.FAILED)
            {
                customer.DeviceList.Remove(device);
                device.Present = false;
                customer.DeviceList.Add(device);
            }

            connection.LastConnectionState = connectionState;

            // Update database
            if (!noUpdate)
            {
                PostgreSQL.Instance.SaveCustomer(customer);
            }

            return connectionState;
        }

        // Check if device is given device class
        private bool IsDeviceClass(int value, DeviceClass deviceClass)
        {
            return (value & (1 << (int)deviceClass)) != 0;
        }
        private bool SupportsLightMode(int value, LightMode lightMode)
        {
            return (value & (1 << (int)lightMode)) != 0;
        }

        // Create challenge response to log in
        private string CreateResponse(string challenge, string password)
        {
            string response = challenge + "-" + password;

            StringBuilder stringBuilder = new StringBuilder();

            using (MD5 md5Hasher = MD5.Create())
            {
                byte[] hashByteArray = md5Hasher.ComputeHash(Encoding.Unicode.GetBytes(response));
                
                foreach (byte hashByte in hashByteArray)
                    stringBuilder.Append(hashByte.ToString("x2"));
            }

            return challenge + "-" + stringBuilder;
        }

        // Get value from received XML file
        private string GetXMLValue(string xml, string key)
        {
            Logger.Instance.Debug("Trying to read key {0}", key);
            string value = string.Empty;
            try
            {
                XDocument document = XDocument.Parse(xml);
                XElement element = document.Descendants(key).FirstOrDefault();
                if (element != null)
                {
                    value = element.Value;
                }

                Logger.Instance.Debug("Value found: {0}", value);
            }
            catch (System.Xml.XmlException)
            {
                Logger.Instance.Debug("Exception while reading XML document.");
            }

            return value;
        }

        public static string StripUnicodeCharactersFromString(string inputValue)
        {
            return new string(inputValue.Where(character => character <= byte.MaxValue).ToArray());
        }
    }
}