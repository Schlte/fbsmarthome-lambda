﻿namespace FBSmartHome
{
    using Newtonsoft.Json;
    using RabbitMQ.Client;
    using System;
    using System.Collections.Generic;
    using System.Security.Authentication;
    using System.Text;
    using System.Threading.Tasks;

    public sealed class FritzBoxViaRabbitMQ : IFritzBox
    {
        public static readonly FritzBoxViaRabbitMQ Instance = new FritzBoxViaRabbitMQ();

        private readonly ConnectionFactory connectionFactory;
        private IConnection connection;
        private IModel channel;

        private const string QUEUE = "fbsmarthome";
        private const string EXCHANGE = "fbsmarthome-exchange";
        private const uint QUEUE_SIZE_HEALTHY = 10;

        static FritzBoxViaRabbitMQ()
        {
        }

        private FritzBoxViaRabbitMQ()
        {
            connectionFactory = new ConnectionFactory()
            {
                HostName = Environment.GetEnvironmentVariable("MQ_HOSTNAME"),
                UserName = Environment.GetEnvironmentVariable("MQ_USERNAME"),
                Password = Environment.GetEnvironmentVariable("MQ_PASSWORD"),
                Port = 5671,
                RequestedConnectionTimeout = TimeSpan.FromMilliseconds(5000),
                SocketReadTimeout = TimeSpan.FromMilliseconds(5000),
                SocketWriteTimeout = TimeSpan.FromMilliseconds(5000),
                AutomaticRecoveryEnabled = true,
                Ssl = new SslOption()
                {
                    Enabled = true,
                    Version = SslProtocols.Tls12,
                    ServerName = Environment.GetEnvironmentVariable("MQ_SERVERNAME")
                }
            };
        }
        public bool IsQueueOK()
        {
            try
            {
                if (connection?.IsOpen != true)
                {
                    connection = connectionFactory.CreateConnection();
                }

                if (channel?.IsOpen != true)
                {
                    channel = connection.CreateModel();
                }

                uint messageCount = channel.MessageCount(QUEUE);
                Logger.Instance.Debug("Rabbit mq queue size: {0}", messageCount);

                if (messageCount <= QUEUE_SIZE_HEALTHY)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                // Exception while publishing to queue
                Logger.Instance.Debug("Exception on rabbit mq publish: {0}", ex);

                return false;
            }

            return false;
        }

        private ConnectionState GetDevices(Customer customer, Connection deviceConnection)
        {
            Logger.Instance.Debug("Time: {0}", deviceConnection.LastUpdatedDevices.ToString());
            // Only update every 20s, Alexa sometimes requesting discovery way too often
            if (deviceConnection.LastUpdatedDevices.AddSeconds(20) > DateTime.UtcNow)
            {
                // else return last connection state
                return deviceConnection.LastConnectionState;
            }

            // set update time
            deviceConnection.LastUpdatedDevices = DateTime.UtcNow;

            // create data
            Dictionary<string, string> message = new Dictionary<string, string>
            {
                // add message type
                { "message", "GetDeviceList" },
                // add customer
                { "customer", customer.CustomerId }
            };

            // create message
            string messageString = JsonConvert.SerializeObject(message);
            byte[] body = Encoding.UTF8.GetBytes(messageString);

            // Publishing to queue
            Logger.Instance.Debug("Trying to put into queue: Get Device List");

            try
            {
                if (connection?.IsOpen != true)
                {
                    connection = connectionFactory.CreateConnection();
                }

                if (channel?.IsOpen != true)
                {
                    channel = connection.CreateModel();
                }

                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:getdevices",
                        DateTimeOffset.Now.ToUnixTimeSeconds(), 1, deviceConnection.UseIPv6);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Expiration = "600000";

                channel.BasicPublish(EXCHANGE, QUEUE, properties, body);
            }
            catch (Exception ex)
            {
                // Exception while publishing to queue
                Logger.Instance.Debug("Exception on rabbit mq publish: {0}", ex);

                return ConnectionState.EXCEPTION;
            }

            return deviceConnection.LastConnectionState;
        }

        public async Task<ConnectionState> GetDevicesAsync(Customer customer, Connection connection, bool saveCustomer, bool forceUpdate)
        {
            ConnectionState connectionState = await Task.FromResult(GetDevices(customer, connection)).ConfigureAwait(false);

            return connectionState;
        }

        private ConnectionState HandleStateReport(string correlationToken, Customer customer, Device device)
        {
            // create data
            Dictionary<string, string> message = new Dictionary<string, string>
            {
                // add message type
                { "message", "ReportState" },
                // add correlation token
                { "correlationToken", correlationToken },
                // add customer
                { "customer", customer.CustomerId },
                // add device
                { "device", device.Identifier }
            };

            // create message
            string messageString = JsonConvert.SerializeObject(message);
            byte[] body = Encoding.UTF8.GetBytes(messageString);

            // Publishing to queue
            Logger.Instance.Debug("Trying to put into queue: Handle State Report");

            try
            {
                if (connection?.IsOpen != true)
                {
                    connection = connectionFactory.CreateConnection();
                }

                if (channel?.IsOpen != true)
                {
                    channel = connection.CreateModel();
                }

                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:statereport,device:statereport",
                    DateTimeOffset.Now.ToUnixTimeSeconds(), 1, customer.GetConnection(device.ConnectionNumber).UseIPv6);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Expiration = "600000";
                properties.Priority = 1;

                channel.BasicPublish(EXCHANGE, QUEUE, properties, body);
            }
            catch (Exception ex)
            {
                // Exception while publishing to queue
                Logger.Instance.Debug("Exception on rabbit mq publish: {0}", ex);

                return ConnectionState.EXCEPTION;
            }

            return customer.GetConnection(device.ConnectionNumber).LastConnectionState;
        }

        public async Task<ConnectionState> HandleStateReportAsync(string correlationToken, Customer customer, Device device)
        {
            ConnectionState connectionState = await Task.FromResult(HandleStateReport(correlationToken, customer, device)).ConfigureAwait(false);

            return connectionState;
        }

        private ConnectionState HandleTurnOn(Device device, Customer customer)
        {
            // create data
            Dictionary<string, string> message = new Dictionary<string, string>
            {
                // add message type
                { "message", "TurnOnDevice" },
                // add customer
                { "customer", customer.CustomerId },
                // add device
                { "device", device.Identifier }
            };

            // create message
            string messageString = JsonConvert.SerializeObject(message);
            byte[] body = Encoding.UTF8.GetBytes(messageString);

            // Publishing to queue
            Logger.Instance.Debug("Trying to put into queue: Turn Device On");

            try
            {
                if (connection?.IsOpen != true)
                {
                    connection = connectionFactory.CreateConnection();
                }

                if (channel?.IsOpen != true)
                {
                    channel = connection.CreateModel();
                }

                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:turnon,device:turnon",
                    DateTimeOffset.Now.ToUnixTimeSeconds(), 1, customer.GetConnection(device.ConnectionNumber).UseIPv6);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Expiration = "600000";
                properties.Priority = 1;

                channel.BasicPublish(EXCHANGE, QUEUE, properties, body);
            }
            catch (Exception ex)
            {
                // Exception while publishing to queue
                Logger.Instance.Debug("Exception on rabbit mq publish: {0}", ex);

                return ConnectionState.EXCEPTION;
            }

            return customer.GetConnection(device.ConnectionNumber).LastConnectionState;
        }

        public async Task<ConnectionState> HandleTurnOnAsync(Device device, Customer customer, bool noUpdate = false)
        {
            ConnectionState connectionState = await Task.FromResult(HandleTurnOn(device, customer)).ConfigureAwait(false);

            return connectionState;
        }

        private ConnectionState HandleTurnOff(Device device, Customer customer)
        {
            // create data
            Dictionary<string, string> message = new Dictionary<string, string>
            {
                // add message type
                { "message", "TurnOffDevice" },
                // add customer
                { "customer", customer.CustomerId},
                // add device
                { "device", device.Identifier }
            };

            // create message
            string messageString = JsonConvert.SerializeObject(message);
            byte[] body = Encoding.UTF8.GetBytes(messageString);

            // Publishing to queue
            Logger.Instance.Debug("Trying to put into queue: Turn Device Off");

            try
            {
                if (connection?.IsOpen != true)
                {
                    connection = connectionFactory.CreateConnection();
                }

                if (channel?.IsOpen != true)
                {
                    channel = connection.CreateModel();
                }

                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:turnoff,device:turnoff",
                    DateTimeOffset.Now.ToUnixTimeSeconds(), 1, customer.GetConnection(device.ConnectionNumber).UseIPv6);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Expiration = "600000";
                properties.Priority = 1;

                channel.BasicPublish(EXCHANGE, QUEUE, properties, body);
            }
            catch (Exception ex)
            {
                // Exception while publishing to queue
                Logger.Instance.Debug("Exception on rabbit mq publish: {0}", ex);

                return ConnectionState.EXCEPTION;
            }

            return customer.GetConnection(device.ConnectionNumber).LastConnectionState;
        }

        public async Task<ConnectionState> HandleTurnOffAsync(Device device, Customer customer, bool noUpdate = false)
        {
            ConnectionState connectionState = await Task.FromResult(HandleTurnOff(device, customer)).ConfigureAwait(false);

            return connectionState;
        }

        private ConnectionState HandleSetTemperature(Device device, double temperature, Customer customer)
        {
            // create data
            Dictionary<string, string> message = new Dictionary<string, string>
            {
                // add message type
                { "message", "SetTemperature" },
                // add customer
                { "customer", customer.CustomerId },
                // add device
                { "device", device.Identifier },
                // add temperature
                { "temperature", temperature.ToString() }
            };

            // create message
            string messageString = JsonConvert.SerializeObject(message);
            byte[] body = Encoding.UTF8.GetBytes(messageString);

            // Publishing to queue
            Logger.Instance.Debug("Trying to put into queue: Set Temperature");

            try
            {
                if (connection?.IsOpen != true)
                {
                    connection = connectionFactory.CreateConnection();
                }

                if (channel?.IsOpen != true)
                {
                    channel = connection.CreateModel();
                }

                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:settemperature,device:settemperature",
                    DateTimeOffset.Now.ToUnixTimeSeconds(), 1, customer.GetConnection(device.ConnectionNumber).UseIPv6);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Expiration = "600000";
                properties.Priority = 1;

                channel.BasicPublish(EXCHANGE, QUEUE, properties, body);
            }
            catch (Exception ex)
            {
                // Exception while publishing to queue
                Logger.Instance.Debug("Exception on rabbit mq publish: {0}", ex);

                return ConnectionState.EXCEPTION;
            }

            return customer.GetConnection(device.ConnectionNumber).LastConnectionState;
        }

        public async Task<ConnectionState> HandleSetTemperatureAsync(Device device, double temperature, Customer customer, bool noUpdate = false)
        {
            ConnectionState connectionState = await Task.FromResult(HandleSetTemperature(device, temperature, customer)).ConfigureAwait(false);

            return connectionState;
        }

        private ConnectionState HandleApplyTemplate(Device device, Customer customer)
        {
            // create data
            Dictionary<string, string> message = new Dictionary<string, string>
            {
                // add message type
                { "message", "ApplyTemplate" },
                // add customer
                { "customer", customer.CustomerId },
                // add device
                { "device", device.Identifier }
            };

            // create message
            string messageString = JsonConvert.SerializeObject(message);
            byte[] body = Encoding.UTF8.GetBytes(messageString);

            // Publishing to queue
            Logger.Instance.Debug("Trying to put into queue: Apply Template");

            try
            {
                if (connection?.IsOpen != true)
                {
                    connection = connectionFactory.CreateConnection();
                }

                if (channel?.IsOpen != true)
                {
                    channel = connection.CreateModel();
                }

                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:applytemplate,device:applytemplate",
                    DateTimeOffset.Now.ToUnixTimeSeconds(), 1, customer.GetConnection(device.ConnectionNumber).UseIPv6);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Expiration = "600000";
                properties.Priority = 1;

                channel.BasicPublish(EXCHANGE, QUEUE, properties, body);
            }
            catch (Exception ex)
            {
                // Exception while publishing to queue
                Logger.Instance.Debug("Exception on rabbit mq publish: {0}", ex);

                return ConnectionState.EXCEPTION;
            }

            return customer.GetConnection(device.ConnectionNumber).LastConnectionState;
        }

        public async Task<ConnectionState> HandleApplyTemplateAsync(Device device, Customer customer, bool noUpdate = false)
        {
            ConnectionState connectionState = await Task.FromResult(HandleApplyTemplate(device, customer)).ConfigureAwait(false);

            return connectionState;
        }

        private Dictionary<string, ConnectionState> HandleMultipleCommands(Dictionary<string, object> commandList, Customer customer)
        {
            // create data
            Dictionary<string, string> message = new Dictionary<string, string>
            {
                // add message type
                { "message", "MultipleCommands" },
                // add customer
                { "customer", customer.CustomerId },
                // add command list
                { "commandList", JsonConvert.SerializeObject(commandList) }
            };

            // create message
            string messageString = JsonConvert.SerializeObject(message);
            byte[] body = Encoding.UTF8.GetBytes(messageString);

            // Publishing to queue
            Logger.Instance.Debug("Trying to put into queue: Multiple Commands");

            try
            {
                if (connection?.IsOpen != true)
                {
                    connection = connectionFactory.CreateConnection();
                }

                if (channel?.IsOpen != true)
                {
                    channel = connection.CreateModel();
                }

                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:multiplecommands,device:multiplecommands",
                    DateTimeOffset.Now.ToUnixTimeSeconds(), 1, customer.GetConnection(ConnectionNumber.DEFAULT).UseIPv6);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Expiration = "600000";
                properties.Priority = 1;

                channel.BasicPublish(EXCHANGE, QUEUE, properties, body);
            }
            catch (Exception ex)
            {
                // Exception while publishing to queue
                Logger.Instance.Debug("Exception on rabbit mq publish: {0}", ex);

                Dictionary<string, ConnectionState> exception = new Dictionary<string, ConnectionState>
                {
                    { "Exception", ConnectionState.EXCEPTION }
                };
                return exception;
            }

            // Create response list
            Dictionary<string, ConnectionState> responseList = new Dictionary<string, ConnectionState>();

            const ConnectionState CONN = ConnectionState.OK;
            foreach (KeyValuePair<string, object> command in commandList)
                responseList.Add(command.Key, CONN);

            return responseList;
        }

        public async Task<Dictionary<string, ConnectionState>> HandleMultipleCommandsAsync(Dictionary<string, object> commandList, Customer customer)
        {
            Dictionary<string, ConnectionState> answer = await Task.FromResult(HandleMultipleCommands(commandList, customer)).ConfigureAwait(false);

            return answer;
        }

        private ConnectionState Homegraph(Customer customer)
        {
            // create data
            Dictionary<string, string> message = new Dictionary<string, string>
            {
                // add message type
                { "message", "HomeGraph" },
                // add customer
                { "customer", customer.CustomerId }
            };

            // create message
            string messageString = JsonConvert.SerializeObject(message);
            byte[] body = Encoding.UTF8.GetBytes(messageString);

            // Publishing to queue
            Logger.Instance.Debug("Trying to put into queue: Homegraph Report");

            try
            {
                if (connection?.IsOpen != true)
                {
                    connection = connectionFactory.CreateConnection();
                }

                if (channel?.IsOpen != true)
                {
                    channel = connection.CreateModel();
                }

                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:homegraph,device:homegraph",
                    DateTimeOffset.Now.ToUnixTimeSeconds(), 1, customer.GetConnection(ConnectionNumber.DEFAULT).UseIPv6);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Expiration = "600000";
                properties.Priority = 1;

                channel.BasicPublish(EXCHANGE, QUEUE, properties, body);
            }
            catch (Exception ex)
            {
                // Exception while publishing to queue
                Logger.Instance.Debug("Exception on rabbit mq publish: {0}", ex);

                return ConnectionState.EXCEPTION;
            }

            return customer.GetConnection(ConnectionNumber.DEFAULT).LastConnectionState;
        }

        public async Task<ConnectionState> HomeGraphAsync(Customer customer)
        {
            ConnectionState connectionState = await Task.FromResult(Homegraph(customer)).ConfigureAwait(false);

            return connectionState;
        }

        private ConnectionState HandleSetLevel(Device device, double level, Customer customer)
        {
            // create data
            Dictionary<string, string> message = new Dictionary<string, string>
            {
                // add message type
                { "message", "SetLevel" },
                // add customer
                { "customer", customer.CustomerId},
                // add device
                { "device", device.Identifier },
                // add level
                { "level", level.ToString() }
            };

            // create message
            string messageString = JsonConvert.SerializeObject(message);
            byte[] body = Encoding.UTF8.GetBytes(messageString);

            // Publishing to queue
            Logger.Instance.Debug("Trying to put into queue: Set Level");

            try
            {
                if (connection?.IsOpen != true)
                {
                    connection = connectionFactory.CreateConnection();
                }

                if (channel?.IsOpen != true)
                {
                    channel = connection.CreateModel();
                }

                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:setlevel,device:setlevel",
                    DateTimeOffset.Now.ToUnixTimeSeconds(), 1, customer.GetConnection(device.ConnectionNumber).UseIPv6);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Expiration = "600000";
                properties.Priority = 1;

                channel.BasicPublish(EXCHANGE, QUEUE, properties, body);
            }
            catch (Exception ex)
            {
                // Exception while publishing to queue
                Logger.Instance.Debug("Exception on rabbit mq publish: {0}", ex);

                return ConnectionState.EXCEPTION;
            }

            return customer.GetConnection(device.ConnectionNumber).LastConnectionState;
        }
        public async Task<ConnectionState> HandleSetLevelAsync(Device device, double level, Customer customer, bool noUpdate = false)
        {
            ConnectionState connectionState = await Task.FromResult(HandleSetLevel(device, level, customer)).ConfigureAwait(false);

            return connectionState;
        }

        private ConnectionState HandleSetLightColorTemperature(Device device, double colorTemperature, Customer customer)
        {
            // create data
            Dictionary<string, string> message = new Dictionary<string, string>
            {
                // add message type
                { "message", "SetColorTemperature" },
                // add customer
                { "customer", customer.CustomerId},
                // add device
                { "device", device.Identifier },
                // add color temperature
                { "colorTemperature", colorTemperature.ToString() }
            };

            // create message
            string messageString = JsonConvert.SerializeObject(message);
            byte[] body = Encoding.UTF8.GetBytes(messageString);

            // Publishing to queue
            Logger.Instance.Debug("Trying to put into queue: Set Color Temperature");

            try
            {
                if (connection?.IsOpen != true)
                {
                    connection = connectionFactory.CreateConnection();
                }

                if (channel?.IsOpen != true)
                {
                    channel = connection.CreateModel();
                }

                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:setcolortemperature,device:setcolortemperature",
                    DateTimeOffset.Now.ToUnixTimeSeconds(), 1, customer.GetConnection(device.ConnectionNumber).UseIPv6);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Expiration = "600000";
                properties.Priority = 1;

                channel.BasicPublish(EXCHANGE, QUEUE, properties, body);
            }
            catch (Exception ex)
            {
                // Exception while publishing to queue
                Logger.Instance.Debug("Exception on rabbit mq publish: {0}", ex);

                return ConnectionState.EXCEPTION;
            }

            return customer.GetConnection(device.ConnectionNumber).LastConnectionState;
        }
        public async Task<ConnectionState> HandleSetLightColorTemperatureAsync(Device device, double colorTemperature, Customer customer, bool noUpdate = false)
        {
            ConnectionState connectionState = await Task.FromResult(HandleSetLightColorTemperature(device, colorTemperature, customer)).ConfigureAwait(false);

            return connectionState;
        }

        private ConnectionState HandleSetLightColor(Device device, double hue, double saturation, Customer customer)
        {
            // create data
            Dictionary<string, string> message = new Dictionary<string, string>
            {
                // add message type
                { "message", "SetColor" },
                // add customer
                { "customer", customer.CustomerId},
                // add device
                { "device", device.Identifier },
                // add hue
                { "hue", hue.ToString() },
                // add saturation
                { "saturation", saturation.ToString() }
            };

            // create message
            string messageString = JsonConvert.SerializeObject(message);Logger.Instance.Debug("Message: {0}", messageString);
            byte[] body = Encoding.UTF8.GetBytes(messageString);

            // Publishing to queue
            Logger.Instance.Debug("Trying to put into queue: Set Color");

            try
            {
                if (connection?.IsOpen != true)
                {
                    connection = connectionFactory.CreateConnection();
                }

                if (channel?.IsOpen != true)
                {
                    channel = connection.CreateModel();
                }

                // Add custom metric for datadog logging
                Logger.Instance.Log("MONITORING|{0}|{1}|count|fritzbox.access|#ipv6:{2},command:setcolor,device:setcolor",
                    DateTimeOffset.Now.ToUnixTimeSeconds(), 1, customer.GetConnection(device.ConnectionNumber).UseIPv6);

                IBasicProperties properties = channel.CreateBasicProperties();
                properties.Expiration = "600000";
                properties.Priority = 1;

                channel.BasicPublish(EXCHANGE, QUEUE, properties, body);
            }
            catch (Exception ex)
            {
                // Exception while publishing to queue
                Logger.Instance.Debug("Exception on rabbit mq publish: {0}", ex);

                return ConnectionState.EXCEPTION;
            }

            return customer.GetConnection(device.ConnectionNumber).LastConnectionState;
        }
        public async Task<ConnectionState> HandleSetLightColorAsync(Device device, double hue, double saturation, Customer customer, bool noUpdate = false)
        {
            ConnectionState connectionState = await Task.FromResult(HandleSetLightColor(device, hue, saturation, customer)).ConfigureAwait(false);

            return connectionState;
        }
    }
}