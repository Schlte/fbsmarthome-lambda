﻿namespace FBSmartHome.GoogleAssistant.JsonObjects
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class Command
    {
        /// <summary>
        /// Required.
        /// </summary>
        [JsonProperty("devices")]
        [JsonRequired]
        public List<ExecuteDevice> Devices { get; set; }

        /// <summary>
        /// Required. The ordered list of execution commands for the attached ids.
        /// </summary>
        [JsonProperty("execution")]
        [JsonRequired]
        public List<Execution> Execution { get; set; }

        public Command()
        {
            Devices = new List<ExecuteDevice>();

            Execution = new List<Execution>();
        }
    }
}
