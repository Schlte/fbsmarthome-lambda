﻿namespace FBSmartHome.GoogleAssistant.JsonObjects
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class Execution
    {
        /// <summary>
        /// String. Required. The command (see below) to execute, with (usually) accompanying parameters.
        /// </summary>
        [JsonProperty("command")]
        [JsonRequired]
        public string Command { get; set; }

        /// <summary>
        /// Map<string, Object> Optional, but aligned with the parameters for each command
        /// </summary>
        [JsonProperty("params")]
        public Dictionary<string, object> Params { get; set; } = new Dictionary<string, object>();

        /// <summary>
        /// Standardconstructor
        /// </summary>
        public Execution()
        {
        }
    }
}
