﻿namespace FBSmartHome.GoogleAssistant.JsonObjects
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class ResponseCommand
    {
        /// <summary>
        /// Array<String>. Required. Partner device IDs of the response
        /// </summary>
        [JsonProperty("ids")]
        [JsonRequired]
        public List<string> Ids { get; set; }

        /// <summary>
        /// String. Required. 
        /// </summary>
        [JsonProperty("status")]
        [JsonRequired]
        public string Status { get; set; }

        /// <summary>
        /// String. Optional. Expanding ERROR state if needed from the preset error codes, 
        /// which will map to the errors presented to users.
        /// </summary>
        [JsonProperty("errorCode", NullValueHandling = NullValueHandling.Ignore)]
        public string ErrorCode { get; set; }

        /// <summary>
        /// String. Optional. Detailed error which will never be presented to users but may be 
        /// logged or used during development.
        /// </summary>
        [JsonProperty("debugString", NullValueHandling = NullValueHandling.Ignore)]
        public string DebugString { get; set; }

        /// <summary>
        /// Object. Optional, but aligned with per-trait states as in Attributes below. 
        /// These are the states _after_ execution, if available.
        /// </summary>
        [JsonProperty("states")]
        public Dictionary<string, object> States { get; set; } = new Dictionary<string, object>();
    }
}
