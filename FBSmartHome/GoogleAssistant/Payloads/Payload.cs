﻿namespace FBSmartHome.GoogleAssistant.Payloads
{
    /// <summary>
    /// Abstract base class for Payloads
    /// </summary>
    public class Payload
    {
        /// <summary>
        /// Basic Constructor
        /// </summary>
        public Payload()
        {
        }
    }
}
