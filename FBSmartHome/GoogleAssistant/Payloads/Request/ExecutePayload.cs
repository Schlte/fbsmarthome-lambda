﻿namespace FBSmartHome.GoogleAssistant.Payloads.Response
{
    using FBSmartHome.GoogleAssistant.JsonObjects;
    using Newtonsoft.Json;
    using System.Collections.Generic;

    /// <summary>
    /// Execute payload for request
    /// </summary>
    public class ExecutePayload : Payload
    {
        /// <summary>
        /// Required. Each object contains one or more devices to target with the attached commands.
        /// </summary>
        [JsonProperty("commands")]
        [JsonRequired]
        public List<Command> Commands { get; set; }

        /// <summary>
        /// Basic Constructor
        /// </summary>
        public ExecutePayload()
        {
            Commands = new List<Command>();
        }
    }
}
