﻿namespace FBSmartHome.GoogleAssistant.Payloads.Response
{
    using FBSmartHome.GoogleAssistant.JsonObjects;
    using Newtonsoft.Json;
    using System.Collections.Generic;

    /// <summary>
    /// Query payload for response
    /// </summary>
    public class ExecuteResponsePayload : Payload
    {
        /// <summary>
        /// String. Optional. An error code for the entire transaction -- for auth failures and partner 
        /// system unavailability. For individual device errors use the errorCode within the device object.
        /// </summary>
        [JsonProperty("errorCode", NullValueHandling = NullValueHandling.Ignore)]
        public string ErrorCode { get; set; }

        /// <summary>
        /// String. Optional. Detailed error which will never be presented to users but may be logged or used during development.
        /// </summary>
        [JsonProperty("debugString", NullValueHandling = NullValueHandling.Ignore)]
        public string DebugString { get; set; }

        /// <summary>
        /// Array<Object>. Required. Each object contains one or more devices with response details. 
        /// N.B. These may not be grouped the same way as in the request. For example, 
        /// the request might turn 7 lights on, with 3 lights succeeding and 4 failing, thus with 
        /// two groups in the response.
        /// </summary>
        [JsonProperty("commands")]
        [JsonRequired]
        public List<ResponseCommand> Commands { get; set; }

        /// <summary>
        /// Basic Constructor
        /// </summary>
        public ExecuteResponsePayload()
        {
        }
    }
}
