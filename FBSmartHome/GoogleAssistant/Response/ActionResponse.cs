﻿namespace FBSmartHome.GoogleAssistant.Response
{
    using FBSmartHome.GoogleAssistant.Payloads;
    using FBSmartHome.GoogleAssistant.Payloads.Response;
    using FBSmartHome.GoogleAssistant.Request;
    using FBSmartHome.GoogleAssistant.Types;
    using Newtonsoft.Json;
    using System;

    /// <summary>
    /// Exception,if the Intent Name is not known
    /// </summary>
    public class UnknownIntentException : Exception
    {
        /// <summary>
        /// Exception,if the RequestHeader Name is not known
        /// </summary>
        /// <param name="intent"> Name of the Intent</param>
        public UnknownIntentException(string intent) : base(intent + " is not defined") { }

        public UnknownIntentException() : base()
        {
        }

        public UnknownIntentException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }

    /// <summary>
    /// Response to Google Assistant
    /// </summary>
    public class ActionResponse
    {
        /// <summary>
        /// RequestId
        /// </summary>
        [JsonProperty("requestId")]
        [JsonRequired]
        public string RequestId { get; set; }

        /// <summary>
        /// Payload
        /// </summary>
        [JsonProperty("payload")]
        public Payload Payload { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="actionRequest">Received Action Request</param>
        public ActionResponse(ActionRequest actionRequest)
        {
            RequestId = actionRequest.RequestId;
            Payload = _GetPayloadForRequest(actionRequest.Inputs[0].Intent);
        }

        public static ActionResponse GetErrorResponse(ActionRequest actionRequest)
        {
            ActionResponse response = new ActionResponse
            {
                RequestId = actionRequest.RequestId,
                Payload = new ActionErrorPayload()
            };

            return response;
        }

        /// <summary>
        /// Basic Constructor
        /// </summary>
        public ActionResponse()
        {
        }

        internal Payload _GetPayloadForRequest(string intent)
        {
            switch (intent)
            {
                case IntentNames.SYNC:
                    return new SyncPayload();
                case IntentNames.QUERY:
                    return new QueryResponsePayload();
                case IntentNames.EXECUTE:
                    return new ExecuteResponsePayload();
                case IntentNames.DISCONNECT:
                    return new DisconnectResponsePayload();
                default:
                    throw new UnknownIntentException(intent);
            }
        }
    }
}
