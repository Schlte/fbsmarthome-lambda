﻿namespace FBSmartHome.GoogleAssistant.Types
{
    /// <summary>
    /// Intents
    /// </summary>
    public static class Attributes
    {
        /// <summary>
        /// Comma-separated list of modes supported by this specific device. Currently 
        /// supports specific known modes from the following list; more and custom ones 
        /// will be added as needed. These modes all have local expansion aliases
        /// off Heating/Cooling activity disabled.
        /// heat If the device supports heating.
        /// cool If the device supports cooling.
        /// on If off, on restores the previous mode of the device.
        /// heatcool If the device supports maintaining heating/cooling to target a range.
        /// </summary>
        public const string THERMOSTAT_MODES = "availableThermostatModes";

        /// <summary>
        /// C or F. The unit the device is set to by default. The device "speaks" using its display unit.
        /// </summary>
        public const string TEMPERATURE_UNIT = "thermostatTemperatureUnit";

        /// <summary>
        /// Boolean. Optional. Required if the device supports query-only execution. This attribute indicates if the 
        /// device can only be queried for state information, and cannot be controlled. Sensors that can only report 
        /// temperature should set this field to true. If this field is true, the availableThermostatModes 
        /// attribute is optional.
        /// </summary>
        public const string QUERYONLY_TEMPERATURESETTING = "queryOnlyTemperatureSetting";

        /// <summary>
        /// Boolean, indicating that this scene may be reversed. This is only relevant for scenes that modify state and 
        /// remember previous state. Obviously, scenes that fire specific actions may not be reversible — the bell cannot be unrung. 
        /// If true, Deactivate Party Mode will function as expected.
        /// </summary>
        public const string SCENE_REVERSIBLE = "sceneReversible";

        /// <summary>
        /// String. Required if the device supports the full spectrum color model. Required if the colorTemperatureRange attribute is not set. 
        /// Must be set to exactly one of the following values:
        /// - rgb Default model.The device supports the RGB color model.
        /// - hsv The device supports the HSV color model.
        /// </summary>
        public const string COLOR_MODEL = "colorModel";

        /// <summary>
        /// Object. Required if the device supports color temperature set by Kelvin. Required if the colorModel attribute is not set. 
        /// Contains the minimum and maximum values in Kelvin supported by the device. Note that colorTemperatureRange may exist when colorModel 
        /// is rgb or hsv if the device supports both temperature and a full spectrum color model.
        /// - temperatureMinK Integer.Minimum color temperature (in Kelvin) supported by the device.
        /// - temperatureMaxK Integer.Maximum color temperature (in Kelvin) supported by the device.
        /// </summary>
        public const string COLOR_TEMPERATURE_RANGE = "colorTemperatureRange";

        /// <summary>
        /// Boolean. Defaults to false. Required if the device supports command-only execution. 
        /// This attribute indicates if the device supports one-way or two-way communication (for example, 
        /// two-way is supported if the controller reports the new state after sending the request). Must be 
        /// set to true if the device cannot confirm successful execution of the request by reporting state.
        /// </summary>
        public const string COMMANDONLY_COLORSETTING = "commandOnlyColorSetting";

        public const string OPENDIRECTION = "openDirection";
    }
}
