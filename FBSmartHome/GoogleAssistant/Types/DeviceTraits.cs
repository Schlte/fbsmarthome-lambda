﻿namespace FBSmartHome.GoogleAssistant.Types
{
    /// <summary>
    /// Intents
    /// </summary>
    public static class DeviceTraits
    {
        /// <summary>
        /// Device Trait Brightness
        /// </summary>
        public const string BRIGHTNESS = "action.devices.traits.Brightness";

        /// <summary>
        /// Device Trait CameraStream
        /// </summary>
        public const string CAMERASTREAM = "action.devices.traits.CameraStream";

        /// <summary>
        /// Device Trait ColorSetting
        /// </summary>
        public const string COLORSETTING = "action.devices.traits.ColorSetting";

        /// <summary>
        /// Device Trait Dock
        /// </summary>
        public const string DOCK = "action.devices.traits.Dock";

        /// <summary>
        /// Device Trait Modes
        /// </summary>
        public const string MODES = "action.devices.traits.Modes";

        /// <summary>
        /// Device Trait OnOff
        /// </summary>
        public const string ONOFF = "action.devices.traits.OnOff";

        /// <summary>
        /// Device Trait RunCycle
        /// </summary>
        public const string RUNCYCLE = "action.devices.traits.RunCycle";

        /// <summary>
        /// Device Trait Scene
        /// </summary>
        public const string SCENE = "action.devices.traits.Scene";

        /// <summary>
        /// Device Trait StartStop
        /// </summary>
        public const string STARTSTOP = "action.devices.traits.StartStop";

        /// <summary>
        /// Device Trait TemperatureSetting
        /// </summary>
        public const string TEMPERATURESETTING = "action.devices.traits.TemperatureSetting";

        /// <summary>
        /// Device Trait Toggles
        /// </summary>
        public const string TOGGLES = "action.devices.traits.Toggles";

        public const string OPENCLOSE = "action.devices.traits.OpenClose";
    }
}
