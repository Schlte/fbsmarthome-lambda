﻿namespace FBSmartHome.GoogleAssistant.Types
{
    /// <summary>
    /// Intents
    /// </summary>
    public static class DeviceType
    {
        /// <summary>
        /// Device Type CAMERA
        /// </summary>
        public const string CAMERA = "action.devices.types.CAMERA";

        /// <summary>
        /// Device Type DISHWASHER
        /// </summary>
        public const string DISHWASHER = "action.devices.types.DISHWASHER";

        /// <summary>
        /// Device Type DRYER
        /// </summary>
        public const string DRYER = "action.devices.types.DRYER";

        /// <summary>
        /// Device Type LIGHT
        /// </summary>
        public const string LIGHT = "action.devices.types.LIGHT";

        /// <summary>
        /// Device Type Camera
        /// </summary>
        public const string OUTLET = "action.devices.types.OUTLET";

        /// <summary>
        /// Device Type Camera
        /// </summary>
        public const string REFRIGERATOR = "action.devices.types.REFRIGERATOR";

        /// <summary>
        /// Device Type Camera
        /// </summary>
        public const string SCENE = "action.devices.types.SCENE";

        /// <summary>
        /// Device Type Camera
        /// </summary>
        public const string SWITCH = "action.devices.types.SWITCH";

        /// <summary>
        /// Device Type Camera
        /// </summary>
        public const string THERMOSTAT = "action.devices.types.THERMOSTAT";

        /// <summary>
        /// Device Type Camera
        /// </summary>
        public const string VACUUM = "action.devices.types.VACUUM";

        /// <summary>
        /// Device Type Camera
        /// </summary>
        public const string WASHER = "action.devices.types.WASHER";

        public const string BLINDS = "action.devices.types.BLINDS";
    }
}
