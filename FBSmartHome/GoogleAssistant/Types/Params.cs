﻿namespace FBSmartHome.GoogleAssistant.Types
{
    /// <summary>
    /// Intents
    /// </summary>
    public static class Params
    {
        /// <summary>
        /// Float (in speech we generally take 1 decimal point, for Celsius users).
        /// </summary>
        public const string TEMPERATURE_SETPOINT = "thermostatTemperatureSetpoint";

        /// <summary>
        /// Floats. The high and low set points for a range. This works if and only if 
        /// the device supports heatcool mode, and we will set that mode if a range is requested.
        /// </summary>
        public const string TEMPERATURE_SETPOINT_HIGH = "thermostatTemperatureSetpointHigh";

        /// <summary>
        /// Floats. The high and low set points for a range. This works if and only if 
        /// the device supports heatcool mode, and we will set that mode if a range is requested.
        /// </summary>
        public const string TEMPERATURE_SETPOINT_LOW = "thermostatTemperatureSetpointLow";

        /// <summary>
        /// One of the supported modes on the device.
        /// </summary>
        public const string THERMOSTAT_MODE = "thermostatMode";

        /// <summary>
        /// Boolean. Required. Whether to turn the device on or off.
        /// </summary>
        public const string ON = "on";

        /// <summary>
        /// Boolean. True if scene is reversible and should be reversed.
        /// </summary>
        public const string DEACTIVATE = "deactivate";

        /// <summary>
        /// Integer. Required. New absolute brightness, from 0 to 100.
        /// </summary>
        public const string BRIGHTNESS = "brightness";

        /// <summary>
        /// Object. The new color setting.
        /// </summary>
        public const string COLOR = "color";

        /// <summary>
        /// Integer. Value in Kelvin. Used if the device is currently set to a color temperature; for example, "cool white" at 4000 K.
        /// </summary>
        public const string TEMPERATURE = "temperature";

        /// <summary>
        /// Integer. Used if colorModel is set to rgb. Spectrum value in RGB, which is a hex value converted to a decimal integer. 
        /// For example, "blue", represented as #0000FF in hexadecimal, would map to decimal integer 255.
        /// </summary>
        public const string SPECTRUM_RGB = "spectrumRGB";

        /// <summary>
        /// Object. Used if colorModel is set to hsv. Must contain the following three parameters:
        /// </summary>
        public const string SPECTRUM_HSV = "spectrumHSV";

        /// <summary>
        /// Float representing hue as positive degrees (for example, 300.0° rather than -60.0°) in the range of [0.0, 360.0).
        /// </summary>
        public const string HUE = "hue";

        /// <summary>
        /// Float. Saturation as a percentage in the range [0.0, 1.0].
        /// </summary>
        public const string SATURATION = "saturation";

        /// <summary>
        /// Float. Value as a percentage in the range [0.0, 1.0]
        /// </summary>
        public const string VALUE = "value";

        public const string OPEN_PERCENT = "openPercent";
    }
}
