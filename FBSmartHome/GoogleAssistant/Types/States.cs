﻿namespace FBSmartHome.GoogleAssistant.Types
{
    /// <summary>
    /// Intents
    /// </summary>
    public static class States
    {
        /// <summary>
        /// Device Type CAMERA
        /// </summary>
        public const string ON = "on";

        /// <summary>
        /// Integer. Current brightness level. Ideally, this is the number that was set versus the 
        /// rounded point (for example, if the user sets it to 65%, but the device has to round to 10% increments, 
        /// ideally this would still return 65%).
        /// </summary>
        public const string BRIGHTNESS = "brightness";

        /// <summary>
        /// Object. The current color setting. Must contain exactly one of the following states (whichever is currently being used on the device)
        /// </summary>
        public const string COLOR = "color";

        /// <summary>
        /// Integer. Value in Kelvin. Used if the device is currently set to a color temperature; for example, "cool white" at 4000 K.
        /// </summary>
        public const string TEMPERATURE_K = "temperatureK";

        /// <summary>
        /// Integer. Used if colorModel is set to rgb. Spectrum value in RGB, which is a hex value converted to a decimal integer. 
        /// For example, "blue", represented as #0000FF in hexadecimal, would map to decimal integer 255.
        /// </summary>
        public const string SPECTRUM_RGB = "spectrumRgb";

        /// <summary>
        /// Object. Used if colorModel is set to hsv. Must contain the following three parameters:
        /// </summary>
        public const string SPECTRUM_HSV = "spectrumHsv";

        /// <summary>
        /// Float representing hue as positive degrees (for example, 300.0° rather than -60.0°) in the range of [0.0, 360.0).
        /// </summary>
        public const string HUE = "hue";

        /// <summary>
        /// Float. Saturation as a percentage in the range [0.0, 1.0].
        /// </summary>
        public const string SATURATION = "saturation";

        /// <summary>
        /// Float. Value as a percentage in the range [0.0, 1.0]
        /// </summary>
        public const string VALUE = "value";

        /// <summary>
        /// Current mode of the device, with the same values as the attribute
        /// </summary>
        public const string THERMOSTAT_MODE = "thermostatMode";

        /// <summary>
        /// Current temperature set point (single target), in C
        /// </summary>
        public const string THERMOSTAT_SETPOINT = "thermostatTemperatureSetpoint";

        /// <summary>
        /// Current observe temperature, in C
        /// </summary>
        public const string THERMOSTAT_TEMPERATURE = "thermostatTemperatureAmbient";

        /// <summary>
        /// Current high point if in heatcool mode, for a range
        /// </summary>
        public const string THERMOSTAT_SETPOINT_HIGH = "thermostatTemperatureSetpointHigh";

        /// <summary>
        /// Current low point if in heatcool mode, for a range
        /// </summary>
        public const string THERMOSTAT_SETPOINT_LOW = "thermostatTemperatureSetpointLow";

        /// <summary>
        /// Float. 0-100. If available from the device.
        /// </summary>
        public const string THERMOSTAT_HUMIDITY = "thermostatHumidityAmbient";

        /// <summary>
        /// Bool. Device is online
        /// </summary>
        public const string ONLINE = "online";

        /// <summary>
        /// You should return an exception when there is an issue or alert associated with a command. The command could succeed or fail.
        /// </summary>
        public const string EXCEPTIONCODE = "exceptionCode";

        public const string OPEN = "openState";
        public const string OPEN_PERCENT = "openPercent";
        public const string OPEN_DIRECTION = "openDirection";
    }
}
