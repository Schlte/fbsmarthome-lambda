﻿namespace FBSmartHome.GoogleAssistant.Types
{
    /// <summary>
    /// Intents
    /// </summary>
    public static class Status
    {
        /// <summary>
        /// SUCCESS - confirmed that the command(s) has/have succeeded
        /// </summary>
        public const string SUCCESS = "SUCCESS";

        /// <summary>
        /// PENDING - commands are enqueued but expected to succeed.
        /// </summary>
        public const string PENDING = "PENDING";

        /// <summary>
        /// OFFLINE - target devices(s) in offline state or unreachable.
        /// </summary>
        public const string OFFLINE = "OFFLINE";

        /// <summary>
        /// ERROR - unable to perform the commands.
        /// </summary>
        public const string ERROR = "ERROR";
    }
}
