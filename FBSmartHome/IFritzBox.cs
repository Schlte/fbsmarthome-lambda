﻿namespace FBSmartHome
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IFritzBox
    {
        Task<ConnectionState> GetDevicesAsync(Customer customer, Connection connection, bool saveCustomer, bool forceUpdate);

        Task<ConnectionState> HandleTurnOnAsync(Device device, Customer customer, bool noUpdate = false);

        Task<ConnectionState> HandleTurnOffAsync(Device device, Customer customer, bool noUpdate = false);

        Task<ConnectionState> HandleSetTemperatureAsync(Device device, double temperature, Customer customer, bool noUpdate = false);

        Task<ConnectionState> HandleApplyTemplateAsync(Device device, Customer customer, bool noUpdate = false);

        Task<ConnectionState> HandleSetLevelAsync(Device device, double level, Customer customer, bool noUpdate = false);

        Task<ConnectionState> HandleSetLightColorTemperatureAsync(Device device, double colorTemperature, Customer customer, bool noUpdate = false);

        Task<ConnectionState> HandleSetLightColorAsync(Device device, double hue, double saturation, Customer customer, bool noUpdate = false);

        Task<Dictionary<string, ConnectionState>> HandleMultipleCommandsAsync(Dictionary<string, object> commandList, Customer customer);
    }
}
