﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace RKon.Alexa.NET46.JsonObjects
{
    public class ActionMapping
    {
        [JsonProperty("@type")]
        [JsonRequired]
        public string Type { get; set; }

        [JsonProperty("actions")]
        [JsonRequired]
        public List<string> Actions { get; set; }

        [JsonProperty("directive")]
        [JsonRequired]
        public ActionMappingDirective Directive { get; set; }

        public ActionMapping(string type, List<string> actions, ActionMappingDirective directive)
        {
            Type = type;
            Actions = actions;
            Directive = directive;
        }
    }
}