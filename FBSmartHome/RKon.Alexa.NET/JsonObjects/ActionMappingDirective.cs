﻿using Newtonsoft.Json;
using RKon.Alexa.NET46.Types;

namespace RKon.Alexa.NET46.JsonObjects
{
    public class ActionMappingDirective
    {
        [JsonProperty("name")]
        [JsonRequired]
        public string Name { get; set; }

        [JsonProperty("payload")]
        [JsonRequired]
        public ActionMappingPayload Payload { get; set; }

        public ActionMappingDirective(string name, ActionMappingPayload payload)
        {
            Name = name;
            Payload = payload;
        }
    }
}