﻿using Newtonsoft.Json;
using RKon.Alexa.NET46.Types;

namespace RKon.Alexa.NET46.JsonObjects
{
    public class ActionMappingPayload
    {
        [JsonProperty("rangeValue", NullValueHandling = NullValueHandling.Ignore)]
        public int RangeValue { get; set; }

        [JsonProperty("rangeValueDelta", NullValueHandling = NullValueHandling.Ignore)]
        public int RangeValueDelta { get; set; }

        [JsonProperty("rangeValueDeltaDefault", NullValueHandling = NullValueHandling.Ignore)]
        public bool RangeValueDeltaDefault { get; set; }

        private readonly bool isValue = false;
        private readonly bool isValueDelta = false;

        public bool ShouldSerializeRangeValue() => isValue;
        public bool ShouldSerializeRangeValueDelta() => isValueDelta;
        public bool ShouldSerializeRangeValueDeltaDefault() => isValueDelta;

        public ActionMappingPayload(int rangeValue)
        {
            RangeValue = rangeValue;
            isValue = true;
        }

        public ActionMappingPayload(int rangeValueDelta, bool rangeValueDeltaDefault)
        {
            RangeValueDelta = rangeValueDelta;
            RangeValueDeltaDefault = rangeValueDeltaDefault;
            isValueDelta = true;
        }
    }
}