﻿using Newtonsoft.Json;

namespace RKon.Alexa.NET46.JsonObjects
{
    /// <summary>
    /// The additionalAttributes object contains additional information about an endpoint. 
    /// Alexa can use this information to identify devices when different skills describe the same device in different ways.
    /// </summary>
    public class AdditionalAttributes
    {
        /// <summary>
        /// The name of the manufacturer of the device. This value can contain up to 256 alphanumeric characters, and can contain punctuation.
        /// </summary>
        [JsonProperty("manufacturer", NullValueHandling = NullValueHandling.Ignore)]
        public string Manufacturer { get; set; }

        /// <summary>
        /// The name of the model of the device. This value can contain up to 256 alphanumeric characters, and can contain punctuation.
        /// </summary>
        [JsonProperty("model", NullValueHandling = NullValueHandling.Ignore)]
        public string Model { get; set; }

        /// <summary>
        /// The serial number of the device. This value can contain up to 256 alphanumeric characters, and can contain punctuation.
        /// </summary>
        [JsonProperty("serialNumber", NullValueHandling = NullValueHandling.Ignore)]
        public string SerialNumber { get; set; }

        /// <summary>
        /// The firmware version of the device. This value can contain up to 256 alphanumeric characters, and can contain punctuation.
        /// </summary>
        [JsonProperty("firmwareVersion", NullValueHandling = NullValueHandling.Ignore)]
        public string FirmwareVersion { get; set; }

        /// <summary>
        /// The software version of the device. This value can contain up to 256 alphanumeric characters, and can contain punctuation.
        /// </summary>
        [JsonProperty("softwareVersion", NullValueHandling = NullValueHandling.Ignore)]
        public string SoftwareVersion { get; set; }

        /// <summary>
        /// Your custom identifier for the device. This identifier should be globally unique in your systems across different customer accounts. 
        /// This value can contain up to 256 alphanumeric characters, and can contain punctuation.
        /// </summary>
        [JsonProperty("customIdentifier", NullValueHandling = NullValueHandling.Ignore)]
        public string CustomIdentifier { get; set; }

        /// <summary>
        /// Standardconstructor
        /// </summary>
        public AdditionalAttributes()
        {
        }
    }
}