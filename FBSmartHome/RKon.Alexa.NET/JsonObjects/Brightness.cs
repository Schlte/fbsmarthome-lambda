﻿using Newtonsoft.Json;

namespace RKon.Alexa.NET46.JsonObjects
{
    /// <summary>
    /// Brightness with limit between 0 and 100
    /// </summary>
    public class Brightness
    {
        private double mValue;

        /// <summary>
        /// value
        /// </summary>
        [JsonRequired]
        [JsonProperty("value")]
        public double Value
        {
            get
            {
                return mValue;
            }
            set
            {
                mValue = value;
                if (value < 0)
                    mValue = 0;
                if (value > 100)
                    mValue = 100;
            }
        }
    }
}