﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace RKon.Alexa.NET46.JsonObjects
{
    public class CapabilityResources
    {
        [JsonProperty("friendlyNames")]
        [JsonRequired]
        public List<FriendlyName> FriendlyNames { get; set; }


        public CapabilityResources(List<FriendlyName> friendlyNames)
        {
            FriendlyNames = friendlyNames;
        }
    }
}
