﻿using Newtonsoft.Json;
using RKon.Alexa.NET46.Types;

namespace RKon.Alexa.NET46.JsonObjects
{
    public class FriendlyName
    {
        [JsonProperty("@type")]
        [JsonRequired]
        public FriendlyNameType Type { get; set; }

        [JsonProperty("value")]
        [JsonRequired]
        public FriendlyNameValue Value { get; set; }

        public FriendlyName()
        {
        }

        public FriendlyName(FriendlyNameType type, FriendlyNameValue value)
        {
            Type = type;
            Value = value;
        }
    }
}