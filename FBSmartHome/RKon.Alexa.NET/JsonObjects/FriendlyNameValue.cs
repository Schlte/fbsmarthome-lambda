﻿using Newtonsoft.Json;

namespace RKon.Alexa.NET46.JsonObjects
{
    public class FriendlyNameValue
    {
        [JsonProperty("assetId", NullValueHandling = NullValueHandling.Ignore)]
        public string AssetId { get; set; }

        [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
        public string Text { get; set; }

        [JsonProperty("locale", NullValueHandling = NullValueHandling.Ignore)]
        public string Locale { get; set; }

        public FriendlyNameValue(string assetId)
        {
            AssetId = assetId;
        }
        public FriendlyNameValue(string text, string locale)
        {
            Text = text;
            Locale = locale;
        }
    }
}