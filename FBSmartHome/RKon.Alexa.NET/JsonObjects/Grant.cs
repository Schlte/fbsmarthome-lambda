﻿using Newtonsoft.Json;

namespace RKon.Alexa.NET46.JsonObjects
{
    /// <summary>
    /// An Element for grant directives
    /// </summary>
    public class Grant
    {
        /// <summary>
        /// type of the grant
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// code of the grant
        /// </summary>
        [JsonProperty("code")]
        public string Code { get; set; }
    }
}
