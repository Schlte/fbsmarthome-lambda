﻿using Newtonsoft.Json;

namespace RKon.Alexa.NET46.JsonObjects
{
    /// <summary>
    /// An Element for grant directives
    /// </summary>
    public class Grantee
    {
        /// <summary>
        /// type of the grantee
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// token of the grantee
        /// </summary>
        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
