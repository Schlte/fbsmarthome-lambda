﻿using Newtonsoft.Json;
using RKon.Alexa.NET46.Types;
using System.Collections.Generic;

namespace RKon.Alexa.NET46.JsonObjects
{
    public class Semantics
    {
        [JsonProperty("actionMappings")]
        [JsonRequired]
        public List<ActionMapping> ActionMappings { get; set; }

        [JsonProperty("stateMappings")]
        [JsonRequired]
        public List<StateMapping> StateMappings { get; set; }

        public Semantics(List<ActionMapping> actionMappings, List<StateMapping> stateMappings)
        {
            ActionMappings = actionMappings;
            StateMappings = stateMappings;
        }
    }
}