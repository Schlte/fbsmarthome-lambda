﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace RKon.Alexa.NET46.JsonObjects
{
    public class StateMapping
    {
        [JsonProperty("@type")]
        [JsonRequired]
        public string Type { get; set; }

        [JsonProperty("states")]
        [JsonRequired]
        public List<string> States { get; set; }

        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public int Value { get; set; }

        [JsonProperty("range", NullValueHandling = NullValueHandling.Ignore)]
        public StateMappingRange Range { get; set; }

        public StateMapping(string type, List<string> states, int value)
        {
            Type = type;
            States = states;
            Value = value;
        }

        public StateMapping(string type, List<string> states, StateMappingRange range)
        {
            Type = type;
            States = states;
            Range = range;
        }
    }
}