﻿using Newtonsoft.Json;

namespace RKon.Alexa.NET46.JsonObjects
{
    public class StateMappingRange
    {
        [JsonProperty("minimumValue")]
        [JsonRequired]
        public int MinimumValue { get; set; }

        [JsonProperty("maximumValue")]
        [JsonRequired]
        public int MaximumValue { get; set; }

        public StateMappingRange(int minimumValue, int maximumValue)
        {
            MinimumValue = minimumValue;
            MaximumValue = maximumValue;
        }
    }
}
