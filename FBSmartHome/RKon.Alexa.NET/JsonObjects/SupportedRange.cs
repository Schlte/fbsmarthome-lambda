﻿using Newtonsoft.Json;
using RKon.Alexa.NET46.Types;
using System.Collections.Generic;

namespace RKon.Alexa.NET46.JsonObjects
{
    public class SupportedRange
    {
        [JsonProperty("minimumValue")]
        [JsonRequired]
        public int MinimumValue { get; set; }

        [JsonProperty("maximumValue")]
        [JsonRequired]
        public int MaximumValue { get; set; }

        [JsonProperty("precision")]
        [JsonRequired]
        public int Precision { get; set; }

        public SupportedRange(int minimumValue, int maximumValue, int precision)
        {
            MinimumValue = minimumValue;
            MaximumValue = maximumValue;
            Precision = precision;
        }
    }
}
