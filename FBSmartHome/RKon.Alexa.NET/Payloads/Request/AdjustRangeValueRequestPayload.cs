﻿using Newtonsoft.Json;
namespace RKon.Alexa.NET46.Payloads
{
    /// <summary>
    /// Payload for AdjustRangeValueRequests
    /// </summary>
    public class AdjustRangeValueRequestPayload : Payload
    {
        [JsonProperty("rangeValueDelta")]
        public int rangeValueDelta { get; set; }

        [JsonProperty("rangeValueDeltaDefault")]
        public bool rangeValueDeltaDefault { get; set; }
    }
}
