﻿using Newtonsoft.Json;
using RKon.Alexa.NET46.Types;

namespace RKon.Alexa.NET46.Payloads
{
    /// <summary>
    /// Payload for SetRangeValue
    /// </summary>
    public class SetRangeValueRequestPayload : Payload
    {
        /// <summary>
        /// A value that indicates the desired value.
        /// </summary>
        [JsonProperty(PropertyNames.RANGE_VALUE)]
        public int RangeValue { get; set; }
    }
}
