﻿namespace RKon.Alexa.NET46.Types
{
    /// <summary>
    /// Describes the authorization type. 
    /// </summary>
    public enum CameraAuthorizationTypes
    {
        BASIC,

        DIGEST,

        NONE,

        /// <summary>
        /// Needed? Is in the official sample messages
        /// </summary>
        BEARER
    }
}
