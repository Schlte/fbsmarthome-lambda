﻿
namespace RKon.Alexa.NET46.Types
{
    /// <summary>
    /// Valid camera protocols
    /// </summary>
    public enum CameraProtocols
    {
        /// <summary>
        /// rtsp
        /// </summary>
        RTSP
    }
}
