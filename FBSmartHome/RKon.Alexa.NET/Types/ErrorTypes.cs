﻿
namespace RKon.Alexa.NET46.Types
{
    /// <summary>
    /// Possible error types
    /// </summary>
    public enum ErrorTypes
    {
        /// <summary>
        /// Indicates the grant accept has failed
        /// </summary>
        ACCEPT_GRANT_FAILED,

        /// <summary>
        /// The operation can't be performed because the endpoint is already in operation.
        /// </summary>
        ALREADY_IN_OPERATION,

        /// <summary>
        /// Indicates the target bridge endpoint is currently unreachable or offline.
        /// For example, the bridge might be turned off, 
        /// disconnected from the customer's local area NET46work, or connectivity between the bridge and the device cloud might have been lost.
        /// </summary>
        BRIDGE_UNREACHABLE,

        /// <summary>
        /// The user can't control the device over the internet, and should control the device manually instead.
        /// </summary>
        CLOUD_CONTROL_DISABLED,

        /// <summary>
        /// 	Indicates the target endpoint cannot respond because it is performing another action, which may or may not have originated from a request to Alexa.
        /// </summary>
        ENDPOINT_BUSY,

        /// <summary>
        /// Indicates the directive could not be completed because the target endpoint has low batteries.
        /// </summary>
        ENDPOINT_LOW_POWER,

        /// <summary>
        /// Indicates the target endpoint is currently unreachable or offline. 
        /// For example, the endpoint might be turned off, 
        /// disconnected from the customer's local area NET46work, or connectivity between the endpoint and 
        /// bridge or the endpoint and the device cloud might have been lost.
        /// </summary>
        ENDPOINT_UNREACHABLE,

        /// <summary>
        /// Indicates that the authorization credential provided by Alexa has expired. For example, the OAuth2 access token for that customer has expired.
        /// </summary>
        EXPIRED_AUTHORIZATION_CREDENTIAL,

        /// <summary>
        /// Indicates a directive could not be handled because the firmware for a bridge or an endpoint is out of date.
        /// </summary>
        FIRMWARE_OUT_OF_DATE,

        /// <summary>
        /// Indicates a directive could not be handled because a bridge or an endpoint has experienced a hardware malfunction.
        /// </summary>
        HARDWARE_MALFUNCTION,

        /// <summary>
        /// Alexa does not have permissions to perform the specified action on the endpoint.
        /// </summary>
        INSUFFICIENT_PERMISSIONS,

        /// <summary>
        /// Indicates an error that cannot be accurately described as one of the other error types occurred while you were handling the directive.
        /// For example, a generic runtime exception occurred while handling a directive. 
        /// Ideally, you will never send this error event, but instead send a more specific error type. 
        /// </summary>
        INTERNAL_ERROR,

        /// <summary>
        /// Indicates that the authorization credential provided by Alexa is invalid. For example, the OAuth2 access token is not valid for the customer's device cloud account. 
        /// </summary>
        INVALID_AUTHORIZATION_CREDENTIAL,

        /// <summary>
        /// Indicates a directive is not valid for this skill or is malformed.
        /// </summary>
        INVALID_DIRECTIVE,

        /// <summary>
        /// Indicates a directive contains an invalid value for the target endpoint.
        /// For example, use to indicate a request with an invalid heating mode, channel value or program value.
        /// </summary>
        INVALID_VALUE,

        /// <summary>
        /// Indicates the target endpoint does not exist or no longer exists.
        /// </summary>
        NO_SUCH_ENDPOINT,

        /// <summary>
        /// The endpoint can't handle the directive because it is in a calibration phase, 
        /// such as warming up, or a user configuration is not set up yet on the device.
        /// </summary>
        NOT_CALIBRATED,

        /// <summary>
        /// Indicates the target endpoint cannot be set to the specified value because of its current mode of operation.
        /// </summary>
        NOT_SUPPORTED_IN_CURRENT_MODE,

        /// <summary>
        /// Indicates the maximum rate at which an endpoint or bridge can process directives has been exceeded.
        /// </summary>
        RATE_LIMIT_EXCEEDED,

        /// <summary>
        /// The endpoint is not in operation. For example, a smart home skill can return a NOT_IN_OPERATION 
        /// error when it receives a RESUME directive but the endpoint is the OFF mode.
        /// </summary>
        NOT_IN_OPERATION,

        /// <summary>
        /// The endpoint can't handle the directive because it doesn't support the requested power level.
        /// </summary>
        POWER_LEVEL_NOT_SUPPORTED,

        /// <summary>
        /// Indicates a directive that contains a value that outside the numeric temperature range accepted for the target thermostat.
        /// For more thermostat-specific errors, see the error section of the Alexa.ThermostatController interface. 
        /// Note that the namespace for thermostat-specific errors is Alexa.ThermostatController
        /// </summary>
        TEMPERATURE_VALUE_OUT_OF_RANGE,

        /// <summary>
        /// The number of allowed failed attempts, such as when entering a password, has been exceeded.
        /// </summary>
        TOO_MANY_FAILED_ATTEMPTS,

        /// <summary>
        /// Indicates a directive contains a value that is outside the numerical range accepted for the target endpoint.
        /// For example, use to respond to a request to set a percentage value over 100 percent. For temperature values, use TEMPERATURE_VALUE_OUT_OF_RANGE
        /// </summary>
        VALUE_OUT_OF_RANGE,

        /// <summary>
        ///  	Thermostat is off and cannot be turned on.
        /// </summary>
        THERMOSTAT_IS_OFF,

        /// <summary>
        /// Setpoints are too close together.
        /// </summary>
        REQUESTED_SETPOINTS_TOO_CLOSE,

        /// <summary>
        /// The thermostat doesn’t support the specified mode.
        /// </summary>
        UNSUPPORTED_THERMOSTAT_MODE,

        /// <summary>
        /// The thermostat doesn’t support dual setpoints in the current mode.
        /// </summary>
        DUAL_SETPOINTS_UNSUPPORTED,

        /// <summary>
        /// The thermostat doesn’t support triple setpoints in the current mode.
        /// </summary>
        TRIPLE_SETPOINTS_UNSUPPORTED,

        /// <summary>
        /// You cannot set the value because it may cause damage to the device or appliance.
        /// </summary>
        UNWILLING_TO_SET_VALUE
    }
}
