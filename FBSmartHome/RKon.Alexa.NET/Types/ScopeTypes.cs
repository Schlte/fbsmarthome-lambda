﻿
namespace RKon.Alexa.NET46.Types
{
    /// <summary>
    /// Supported Types of Scopes
    /// </summary>
    public enum ScopeTypes
    {
        /// <summary>
        /// Provides an OAuth bearer token for accessing a linked customer account or identifying an Alexa customer.
        /// </summary>
        BearerToken
    }
}
