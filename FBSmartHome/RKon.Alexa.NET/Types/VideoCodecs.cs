﻿namespace RKon.Alexa.NET46.Types
{
    /// <summary>
    /// The video codec for the stream.
    /// </summary>
    public enum VideoCodecs
    {
        H264,

        MPEG2,

        MJPEG,

        JPG
    }
}
