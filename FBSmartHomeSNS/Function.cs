namespace FBSmartHomeSNS
{
    using Amazon.Lambda.SNSEvents;
    using FBSmartHome;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using static Amazon.Lambda.SNSEvents.SNSEvent;

    public class Function
    {
        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        /// 
        public Stream FunctionHandler(Stream input)
        {
            DateTime startTime = DateTime.UtcNow;

            // Read input
            SNSEvent snsEvent;

            using (TextReader textReader = new StreamReader(input))
            {
                snsEvent = JsonConvert.DeserializeObject<SNSEvent>(textReader.ReadToEnd());
                Logger.Instance.Debug("Received request: {0}", JsonConvert.SerializeObject(snsEvent, new StringEnumConverter()));
            }

            // Handle Event
            ConnectionState connection = EventHandlerAsync(snsEvent).GetAwaiter().GetResult();

            string responseString = "Fail";
            if (connection == ConnectionState.OK || connection == ConnectionState.FAILED)
            {
                responseString = "OK";
            }

            // Add custom metric for datadog logging
            double timeElapsed = (DateTime.UtcNow - startTime).TotalMilliseconds;
            double timeBilled = Math.Ceiling(timeElapsed / 100.0) * 100;
            Logger.Instance.Log("MONITORING|{0}|{1}|count|lambda.time.elapsed", DateTimeOffset.Now.ToUnixTimeSeconds(), timeElapsed);
            Logger.Instance.Log("MONITORING|{0}|{1}|count|lambda.time.billed", DateTimeOffset.Now.ToUnixTimeSeconds(), timeBilled);

            // Return response
            Logger.Instance.Debug("Sending response: {0}", responseString);
            return new MemoryStream(Encoding.UTF8.GetBytes(responseString));
        }

        public async Task<ConnectionState> EventHandlerAsync(SNSEvent snsEvent)
        {
            ConnectionState connectionState = ConnectionState.UNKNOWN;

            foreach (SNSRecord record in snsEvent.Records)
            {
                record.Sns.MessageAttributes.TryGetValue("customer", out MessageAttribute customerAttribute);
                Customer customer = JsonConvert.DeserializeObject<Customer>(customerAttribute?.Value);
                Logger.Instance.Log("Customer: {0}", customer.CustomerId);

                if (record.Sns.Message == "GetDeviceList")
                {
                    Logger.Instance.Debug("Getting device list");

                    record.Sns.MessageAttributes.TryGetValue("connection", out MessageAttribute connectionAttribute);
                    //Connection connection = JsonConvert.DeserializeObject<Connection>(connectionAttribute?.Value);

                    foreach (Connection connection in customer.ConnectionList)
                    {
                        connectionState = await FritzBox.Instance.GetDevicesAsync(customer, connection, false, false).ConfigureAwait(false);
                    }
                    PostgreSQL.Instance.SaveCustomer(customer);
                }
                else if (record.Sns.Message == "TurnOnDevice")
                {
                    Logger.Instance.Debug("Turn on device");

                    record.Sns.MessageAttributes.TryGetValue("device", out MessageAttribute deviceAttribute);
                    Device device = JsonConvert.DeserializeObject<Device>(deviceAttribute?.Value);

                    connectionState = await FritzBox.Instance.HandleTurnOnAsync(device, customer).ConfigureAwait(false);
                }
                else if (record.Sns.Message == "TurnOffDevice")
                {
                    Logger.Instance.Debug("Turn off device");

                    record.Sns.MessageAttributes.TryGetValue("device", out MessageAttribute deviceAttribute);
                    Device device = JsonConvert.DeserializeObject<Device>(deviceAttribute?.Value);

                    connectionState = await FritzBox.Instance.HandleTurnOffAsync(device, customer).ConfigureAwait(false);
                }
                else if (record.Sns.Message == "SetTemperature")
                {
                    Logger.Instance.Debug("Set temperature");

                    record.Sns.MessageAttributes.TryGetValue("device", out MessageAttribute deviceAttribute);
                    Device device = JsonConvert.DeserializeObject<Device>(deviceAttribute?.Value);

                    record.Sns.MessageAttributes.TryGetValue("temperature", out MessageAttribute temperatureAttribute);
                    if (double.TryParse(temperatureAttribute?.Value, out double temperature))
                    {
                        Logger.Instance.Debug("device: {0}, temperature: {1}", device.Identifier, temperature.ToString());
                        connectionState = await FritzBox.Instance.HandleSetTemperatureAsync(device, temperature, customer).ConfigureAwait(false);
                    }
                }
                else if (record.Sns.Message == "ApplyTemplate")
                {
                    Logger.Instance.Debug("Apply template");

                    record.Sns.MessageAttributes.TryGetValue("device", out MessageAttribute deviceAttribute);
                    Device device = JsonConvert.DeserializeObject<Device>(deviceAttribute?.Value);

                    connectionState = await FritzBox.Instance.HandleApplyTemplateAsync(device, customer).ConfigureAwait(false);
                }
                else if (record.Sns.Message == "MultipleCommands")
                {
                    Logger.Instance.Debug("Set Multiple Commands");

                    record.Sns.MessageAttributes.TryGetValue("commandList", out MessageAttribute commandList);
                    Dictionary<string, object> fritzCommands = JsonConvert.DeserializeObject<Dictionary<string, object>>(commandList?.Value);

                    // Call FRITZ!Box to handle multiple commands
                    Dictionary<string, ConnectionState> commandResponse = await FritzBox.Instance.HandleMultipleCommandsAsync(fritzCommands, customer).ConfigureAwait(false);

                    connectionState = ConnectionState.OK;
                    foreach (KeyValuePair<string, ConnectionState> response in commandResponse)
                    {
                        if (response.Value != ConnectionState.OK)
                        {
                            connectionState = response.Value;
                        }
                    }
                }
            }

            return connectionState;
        }
    }
}